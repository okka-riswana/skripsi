// modified for fails webtransport by Marten Richter

// original Copyright by

// Copyright 2021 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This file does not actually implement logging, it merely provides enough of
// logging code for QUICHE to compile.  QUICHE embedders are encouraged to
// override this file with their own logic.  If at some point logging becomes a
// part of Abseil, this file will likely start using that instead.

#ifndef QUICHE_PLATFORM_IMPL_QUICHE_LOGGING_IMPL_H_
#define QUICHE_PLATFORM_IMPL_QUICHE_LOGGING_IMPL_H_

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "absl/base/attributes.h"
#include "glog/logging.h"

namespace quiche {

class NoopLogSink {
 public:
  NoopLogSink() = default;

  template <typename T>
  constexpr explicit NoopLogSink(const T&) {}

  template <typename T1, typename T2>
  constexpr NoopLogSink(const T1&, const T2&) {}

  constexpr std::ostream& stream() { return stream_; }

  // This operator has lower precedence than << but higher than ?:, which is
  // useful for implementing QUICHE_DISREGARD_LOG_STREAM below.
  void operator&(std::ostream&) {}

 private:
  std::stringstream stream_;
};

}  // namespace quiche

// This is necessary because we sometimes call QUICHE_DCHECK inside constexpr
// functions, and then write non-constexpr expressions into the resulting log.
#define QUICHE_DISREGARD_LOG_STREAM(stream) \
  true ? (void)0 : ::quiche::NoopLogSink() & (stream)

#define QUICHE_VLOG_IMPL(verbose_level) VLOG(verbose_level)
#define QUICHE_VLOG_IF_IMPL(verbose_level, condition) \
  VLOG_IF(verbose_level, condition)
#define QUICHE_LOG_FIRST_N_IMPL(severity, n) LOG_FIRST_N(severity, n)
// TODO(okka-riswana): implement QUICHE_LOG_EVERY_N_SEC_IMPL
#define QUICHE_LOG_EVERY_N_SEC_IMPL(severity, seconds) LOG(severity)
#define QUICHE_LOG_IF_IMPL(severity, condition) LOG_IF(severity, condition)

#define QUICHE_LOG_IMPL(severity) QUICHE_LOG_IMPL_##severity()
#define QUICHE_LOG_IMPL_FATAL() LOG(FATAL)
#define QUICHE_LOG_IMPL_DFATAL() DLOG(FATAL)
#define QUICHE_LOG_IMPL_ERROR() LOG(ERROR)
#define QUICHE_LOG_IMPL_WARNING() LOG(WARNING)
#define QUICHE_LOG_IMPL_INFO() LOG(INFO)

#define QUICHE_LOG_INFO_IS_ON_IMPL() true
#define QUICHE_LOG_WARNING_IS_ON_IMPL() true
#define QUICHE_LOG_ERROR_IS_ON_IMPL() true

#define QUICHE_CHECK_INNER_IMPL(condition, details) \
  QUICHE_LOG_IF_IMPL(FATAL, (!(condition))) << details << ". "

#define QUICHE_CHECK_IMPL(condition) \
  QUICHE_CHECK_INNER_IMPL(condition, "CHECK failed: " #condition)

#define QUICHE_CHECK_INNER_IMPL_OP(op, a, b) \
  QUICHE_CHECK_INNER_IMPL(                   \
      (a)op(b),                              \
      "CHECK failed: " #a " (=" << (a) << ") " #op " " #b " (=" << (b) << ")")

#define QUICHE_CHECK_GT_IMPL(a, b) QUICHE_CHECK_INNER_IMPL_OP(>, a, b)
#define QUICHE_CHECK_GE_IMPL(a, b) QUICHE_CHECK_INNER_IMPL_OP(>=, a, b)
#define QUICHE_CHECK_LT_IMPL(a, b) QUICHE_CHECK_INNER_IMPL_OP(<, a, b)
#define QUICHE_CHECK_LE_IMPL(a, b) QUICHE_CHECK_INNER_IMPL_OP(<=, a, b)
#define QUICHE_CHECK_NE_IMPL(a, b) QUICHE_CHECK_INNER_IMPL_OP(!=, a, b)
#define QUICHE_CHECK_EQ_IMPL(a, b) QUICHE_CHECK_INNER_IMPL_OP(==, a, b)

#ifdef NDEBUG
#define QUICHE_COMPILED_OUT_LOG(condition) \
  QUICHE_DISREGARD_LOG_STREAM(quiche::NoopLogSink().stream())
#define QUICHE_DCHECK_IMPL(condition) QUICHE_COMPILED_OUT_LOG(condition)
#define QUICHE_DVLOG_IMPL(verbosity) QUICHE_COMPILED_OUT_LOG(false)
#define QUICHE_DVLOG_IF_IMPL(verbosity, condition) \
  QUICHE_COMPILED_OUT_LOG(condition)
#define QUICHE_DLOG_IMPL(severity) QUICHE_COMPILED_OUT_LOG(false)
#define QUICHE_DLOG_IF_IMPL(severity, condition) \
  QUICHE_COMPILED_OUT_LOG(condition)
#define QUICHE_DLOG_INFO_IS_ON_IMPL() 0
#define QUICHE_DLOG_EVERY_N_IMPL(severity, n) QUICHE_COMPILED_OUT_LOG(false)
#define QUICHE_NOTREACHED_IMPL() LOG(FATAL) << "reached unexpected code"
#define QUICHE_DCHECK_GT_IMPL(a, b) QUICHE_COMPILED_OUT_LOG((a) > (b))
#define QUICHE_DCHECK_GE_IMPL(a, b) QUICHE_COMPILED_OUT_LOG((a) >= (b))
#define QUICHE_DCHECK_LT_IMPL(a, b) QUICHE_COMPILED_OUT_LOG((a) < (b))
#define QUICHE_DCHECK_LE_IMPL(a, b) QUICHE_COMPILED_OUT_LOG((a) <= (b))
#define QUICHE_DCHECK_NE_IMPL(a, b) QUICHE_COMPILED_OUT_LOG((a) != (b))
#define QUICHE_DCHECK_EQ_IMPL(a, b) QUICHE_COMPILED_OUT_LOG((a) == (b))
#else
#define QUICHE_DCHECK_IMPL(condition) QUICHE_CHECK_IMPL(condition)
#define QUICHE_DVLOG_IMPL(verbosity) QUICHE_VLOG_IMPL(verbosity)
#define QUICHE_DVLOG_IF_IMPL(verbosity, condition) \
  QUICHE_VLOG_IF_IMPL(verbosity, condition)
#define QUICHE_DLOG_IMPL(severity) QUICHE_LOG_IMPL(severity)
#define QUICHE_DLOG_IF_IMPL(severity, condition) \
  QUICHE_LOG_IF_IMPL(severity, condition)
#define QUICHE_DLOG_INFO_IS_ON_IMPL() QUICHE_LOG_INFO_IS_ON_IMPL()
#define QUICHE_DLOG_EVERY_N_IMPL(severity, n) \
  QUICHE_LOG_EVERY_N_IMPL(severity, n)
#define QUICHE_NOTREACHED_IMPL() ("reached unexpected code")
#define QUICHE_DCHECK_GT_IMPL(a, b) QUICHE_CHECK_GT_IMPL(a, b)
#define QUICHE_DCHECK_GE_IMPL(a, b) QUICHE_CHECK_GE_IMPL(a, b)
#define QUICHE_DCHECK_LT_IMPL(a, b) QUICHE_CHECK_LT_IMPL(a, b)
#define QUICHE_DCHECK_LE_IMPL(a, b) QUICHE_CHECK_LE_IMPL(a, b)
#define QUICHE_DCHECK_NE_IMPL(a, b) QUICHE_CHECK_NE_IMPL(a, b)
#define QUICHE_DCHECK_EQ_IMPL(a, b) QUICHE_CHECK_EQ_IMPL(a, b)
#endif

#define QUICHE_PREDICT_FALSE_IMPL(x) ABSL_PREDICT_FALSE(x)
#define QUICHE_PREDICT_TRUE_IMPL(x) ABSL_PREDICT_TRUE(x)

#endif  // QUICHE_COMMON_PLATFORM_NODE_QUICHE_PLATFORM_IMPL_QUICHE_LOGGING_IMPL_H_
