#include "web_transport/http3_server_session.h"
#include "quiche/quic/core/http/quic_server_initiated_spdy_stream.h"
#include "quiche/quic/core/quic_utils.h"
#include "web_transport/http3_server_stream.h"

namespace wt {

Http3ServerSession::Http3ServerSession(
    const quic::QuicConfig& config,
    const quic::ParsedQuicVersionVector& supported_versions,
    ::quic::QuicConnection* connection,
    ::quic::QuicSession::Visitor* visitor,
    ::quic::QuicCryptoServerStreamBase::Helper* helper,
    const ::quic::QuicCryptoServerConfig* crypto_config,
    ::quic::QuicCompressedCertsCache* compressed_certs_cache,
    Http3ServerBackend* http3_server_backend)
    : QuicServerSessionBase(config,
                            supported_versions,
                            connection,
                            visitor,
                            helper,
                            crypto_config,
                            compressed_certs_cache),
      highest_promised_stream_id_(
          quic::QuicUtils::GetInvalidStreamId(connection->transport_version())),
      http3_server_backend_(http3_server_backend) {}

Http3ServerSession::~Http3ServerSession() {
  DeleteConnection();
}

void Http3ServerSession::OnStreamFrame(const quic::QuicStreamFrame& frame) {
  if (!IsIncomingStream(frame.stream_id) && !WillNegotiateWebTransport()) {
    LOG(WARNING) << "Client shouldn't send data on server push stream";
    connection()->CloseConnection(
        quic::QUIC_INVALID_STREAM_ID, "Client sent data on server push stream",
        quic::ConnectionCloseBehavior::SEND_CONNECTION_CLOSE_PACKET);
    return;
  }
  QuicSpdySession::OnStreamFrame(frame);
}

void Http3ServerSession::OnCanCreateNewOutgoingStream(bool unidirectional) {
  QuicSpdySession::OnCanCreateNewOutgoingStream(unidirectional);
  if (unidirectional) {
    HandlePromisedPushRequests();
  }
}

quic::QuicSpdyStream* Http3ServerSession::CreateIncomingStream(
    quic::QuicStreamId id) {
  if (!ShouldCreateIncomingStream(id)) {
    return nullptr;
  }

  quic::QuicSpdyStream* stream = new Http3ServerStream(
      id, this, quic::StreamType::BIDIRECTIONAL, http3_server_backend_);
  ActivateStream(absl::WrapUnique(stream));
  return stream;
}
quic::QuicSpdyStream* Http3ServerSession::CreateIncomingStream(
    quic::PendingStream* pending) {
  quic::QuicSpdyStream* stream =
      new Http3ServerStream(pending, this, http3_server_backend_);
  ActivateStream(absl::WrapUnique(stream));
  return stream;
}

quic::QuicSpdyStream* Http3ServerSession::CreateOutgoingBidirectionalStream() {
  if (!WillNegotiateWebTransport()) {
    QUIC_BUG(Http3ServerSession CreateOutgoingBidirectionalStream without
                 WebTransport support)
        << "Http3ServerSession::CreateOutgoingBidirectionalStream called "
           "in a session without WebTransport support.";
    return nullptr;
  }
  if (!ShouldCreateOutgoingBidirectionalStream()) {
    return nullptr;
  }

  quic::QuicServerInitiatedSpdyStream* stream =
      new quic::QuicServerInitiatedSpdyStream(
          GetNextOutgoingBidirectionalStreamId(), this, quic::BIDIRECTIONAL);
  ActivateStream(absl::WrapUnique(stream));
  return stream;
}

quic::QuicSpdyStream* Http3ServerSession::CreateOutgoingUnidirectionalStream() {
  if (!ShouldCreateOutgoingUnidirectionalStream()) {
    return nullptr;
  }

  Http3ServerStream* stream =
      new Http3ServerStream(GetNextOutgoingUnidirectionalStreamId(), this,
                            quic::WRITE_UNIDIRECTIONAL, http3_server_backend_);
  ActivateStream(absl::WrapUnique(stream));
  return stream;
}
void Http3ServerSession::HandleFrameOnNonexistentOutgoingStream(
    quic::QuicStreamId stream_id) {
  // If this stream is a promised but not created stream (stream_id within the
  // range of next_outgoing_stream_id_ and highes_promised_stream_id_),
  // connection shouldn't be closed.
  // Otherwise, behave in the same way as base class.
  if (highest_promised_stream_id_ ==
          quic::QuicUtils::GetInvalidStreamId(transport_version()) ||
      stream_id > highest_promised_stream_id_) {
    QuicSession::HandleFrameOnNonexistentOutgoingStream(stream_id);
  }
}
void Http3ServerSession::HandleRstOnValidNonexistentStream(
    const quic::QuicRstStreamFrame& frame) {
  QuicSession::HandleRstOnValidNonexistentStream(frame);
  if (!IsClosedStream(frame.stream_id)) {
    // If a nonexistent stream is not a closed stream and still valid, it must
    // be a locally preserved stream. Resetting this kind of stream means
    // cancelling the promised server push.
    // Since PromisedStreamInfo are queued in sequence, the corresponding
    // index for it in promised_streams_ can be calculated.
    quic::QuicStreamId next_stream_id =
        next_outgoing_unidirectional_stream_id();
    if ((!version().HasIetfQuicFrames() ||
         !quic::QuicUtils::IsBidirectionalStreamId(frame.stream_id,
                                                   version())) &&
        frame.stream_id >= next_stream_id) {
      size_t index = (frame.stream_id - next_stream_id) /
                     quic::QuicUtils::StreamIdDelta(transport_version());
      if (index < promised_streams_.size()) {
        promised_streams_[index].is_cancelled = true;
      }
    }
    control_frame_manager().WriteOrBufferRstStream(
        frame.stream_id,
        quic::QuicResetStreamError::FromInternal(
            quic::QUIC_RST_ACKNOWLEDGEMENT),
        0);
    connection()->OnStreamReset(frame.stream_id,
                                quic::QUIC_RST_ACKNOWLEDGEMENT);
  }
}
std::unique_ptr<quic::QuicCryptoServerStreamBase>
Http3ServerSession::CreateQuicCryptoServerStream(
    const quic::QuicCryptoServerConfig* crypto_config,
    quic::QuicCompressedCertsCache* compressed_certs_cache) {
  return CreateCryptoServerStream(crypto_config, compressed_certs_cache, this,
                                  stream_helper());
}
void Http3ServerSession::MaybeInitializeHttp3UnidirectionalStreams() {
  size_t previous_static_stream_count = num_static_streams();
  QuicSpdySession::MaybeInitializeHttp3UnidirectionalStreams();
  size_t current_static_stream_count = num_static_streams();
  QUICHE_DCHECK_GE(current_static_stream_count, previous_static_stream_count);
  highest_promised_stream_id_ +=
      quic::QuicUtils::StreamIdDelta(transport_version()) *
      (current_static_stream_count - previous_static_stream_count);
}

void Http3ServerSession::HandlePromisedPushRequests() {
  while (!promised_streams_.empty() &&
         ShouldCreateOutgoingUnidirectionalStream()) {
    PromisedStreamInfo& promised_info = promised_streams_.front();
    QUICHE_DCHECK_EQ(next_outgoing_unidirectional_stream_id(),
                     promised_info.stream_id);

    if (promised_info.is_cancelled) {
      // This stream has been reset by client. Skip this stream id.
      promised_streams_.pop_front();
      GetNextOutgoingUnidirectionalStreamId();
      return;
    }

    Http3ServerStream* promised_stream =
        static_cast<Http3ServerStream*>(CreateOutgoingUnidirectionalStream());
    CHECK_NE(promised_stream, nullptr);
    DCHECK_EQ(promised_info.stream_id, promised_stream->id());
    DLOG(INFO) << "created server push stream " << promised_stream->id();

    promised_stream->SetPriority(promised_info.precedence);

    spdy::Http2HeaderBlock request_headers(
        std::move(promised_info.request_headers));

    promised_streams_.pop_front();
    promised_stream->PushResponse(std::move(request_headers));
  }
}

}  // namespace wt
