#ifndef IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_STREAM_H_
#define IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_STREAM_H_

#include "quiche/quic/core/http/quic_spdy_server_stream_base.h"
#include "web_transport/http3_backend_response.h"
#include "web_transport/http3_server_backend.h"

namespace wt {

class Http3ServerStream : public quic::QuicSpdyServerStreamBase,
                          public Http3ServerBackend::RequestHandler {
 public:
  Http3ServerStream(quic::QuicStreamId id,
                    quic::QuicSpdySession* session,
                    quic::StreamType type,
                    Http3ServerBackend* http3_server_backend);
  Http3ServerStream(quic::PendingStream* pending,
                    quic::QuicSpdySession* session,
                    Http3ServerBackend* http3_server_backend);
  Http3ServerStream(const Http3ServerStream&) = delete;
  Http3ServerStream& operator=(const Http3ServerStream&) = delete;

  ~Http3ServerStream() override;

  // From quic::QuicSpdyStream

  void OnInitialHeadersComplete(
      bool fin,
      size_t frame_len,
      const quic::QuicHeaderList& header_list) override;
  void OnTrailingHeadersComplete(
      bool fin,
      size_t frame_len,
      const quic::QuicHeaderList& header_list) override;
  void OnCanWrite() override;
  // QuicStream implementation called by the sequencer when there is
  // data (or a FIN) to be read.
  void OnBodyAvailable() override;
  void OnInvalidHeaders() override;

  // Make this stream start from as if it just finished parsing an incoming
  // request whose headers are equivalent to |push_request_headers|.
  // Doing so will trigger this stream to fetch response and send it back.
  virtual void PushResponse(spdy::Http2HeaderBlock push_request_headers);

  // The response body of error responses.
  static const char* const kErrorResponseBody;
  static const char* const kNotFoundResponseBody;

  // Implements Http3ServerBackend::RequestHandler callbacks
  quic::QuicConnectionId connection_id() const override;
  quic::QuicStreamId stream_id() const override;
  std::string peer_host() const override;
  void OnResponseBackendComplete(const Http3BackendResponse* response) override;

 protected:
  // Sends a basic 200 response using SendHeaders for the headers and WriteData
  // for the body.
  void SendResponse();

  // Sends a basic 500 response using SendHeaders for the headers and WriteData
  // for the body.
  void SendErrorResponse();
  void SendErrorResponse(int resp_code);

  // Sends a basic 404 response using SendHeaders for the headers and WriteData
  // for the body.
  void SendNotFoundResponse();

  // Sends the response header and body, but not the fin.
  void SendIncompleteResponse(spdy::Http2HeaderBlock response_headers,
                              absl::string_view body);

  void SendHeadersAndBody(spdy::Http2HeaderBlock response_headers,
                          absl::string_view body);
  void SendHeadersAndBodyAndTrailers(spdy::Http2HeaderBlock response_headers,
                                     absl::string_view body,
                                     spdy::Http2HeaderBlock response_trailers);

  // Writes the body bytes for the GENERATE_BYTES response type.
  void WriteGeneratedBytes();

  spdy::Http2HeaderBlock* request_headers() { return &request_headers_; }

  const std::string& body() { return body_; }

  // The parsed headers received from the client.
  spdy::Http2HeaderBlock request_headers_;
  int64_t content_length_;
  std::string body_;

 private:
  uint64_t generate_bytes_length_;
  // Whether response headers have already been sent.
  bool response_sent_ = false;

  Http3ServerBackend* http3_server_backend_;  // Not owned.
};

}  // namespace wt

#endif  // IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_STREAM_H_
