#ifndef IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_BACKEND_RESPONSE_H_
#define IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_BACKEND_RESPONSE_H_

#include "quiche/quic/tools/quic_url.h"
#include "quiche/spdy/core/spdy_header_block.h"
#include "quiche/spdy/core/spdy_protocol.h"

namespace wt {

class Http3BackendResponse {
 public:
  // A ServerPushInfo contains path of the push request and everything needed in
  // comprising a response for the push request.
  struct ServerPushInfo {
    ServerPushInfo(quic::QuicUrl request_url,
                   spdy::Http2HeaderBlock headers,
                   spdy::SpdyPriority priority,
                   std::string body);
    ServerPushInfo(const ServerPushInfo& other);

    quic::QuicUrl request_url;
    spdy::Http2HeaderBlock headers;
    spdy::SpdyPriority priority;
    std::string body;
  };

  enum SpecialResponseType {
    REGULAR_RESPONSE,      // Send the headers and body like a server should.
    CLOSE_CONNECTION,      // Close the connection (sending the close packet).
    IGNORE_REQUEST,        // Do nothing, expect the client to time out.
    BACKEND_ERR_RESPONSE,  // There was an error fetching the response from
                           // the backend, for example as a TCP connection
                           // error.
    INCOMPLETE_RESPONSE,   // The server will act as if there is a non-empty
                           // trailer but it will not be sent, as a result, FIN
                           // will not be sent too.
    GENERATE_BYTES         // Sends a response with a length equal to the number
                           // of bytes in the URL path.
  };

  Http3BackendResponse();
  Http3BackendResponse(const Http3BackendResponse& other) = delete;
  Http3BackendResponse& operator=(const Http3BackendResponse& other) = delete;

  ~Http3BackendResponse();

  void AddEarlyHints(const spdy::Http2HeaderBlock& headers);

  void SetResponseType(SpecialResponseType response_type) {
    response_type_ = response_type;
  }

  void SetHeaders(spdy::Http2HeaderBlock headers) {
    headers_ = std::move(headers);
  }
  void SetTrailers(spdy::Http2HeaderBlock trailers) {
    trailers_ = std::move(trailers);
  }

  void SetBody(absl::string_view body) {
    body_.assign(body.data(), body.size());
  }

  const std::vector<spdy::Http2HeaderBlock>& early_hints() const {
    return early_hints_;
  }
  SpecialResponseType response_type() const { return response_type_; }
  const spdy::Http2HeaderBlock& headers() const { return headers_; }
  const spdy::Http2HeaderBlock& trailers() const { return trailers_; }
  const absl::string_view body() const { return absl::string_view{body_}; }

 private:
  std::vector<spdy::Http2HeaderBlock> early_hints_;
  SpecialResponseType response_type_;
  spdy::Http2HeaderBlock headers_;
  spdy::Http2HeaderBlock trailers_;
  std::string body_;
};

}  // namespace wt

#endif  // IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_BACKEND_RESPONSE_H_
