#include "web_transport/http3_server.h"

#include <memory>

#include "quiche/quic/core/quic_default_packet_writer.h"
#include "quiche/quic/core/quic_epoll_alarm_factory.h"
#include "quiche/quic/core/quic_epoll_clock.h"
#include "quiche/quic/core/quic_epoll_connection_helper.h"
#include "quiche/quic/tools/quic_simple_crypto_server_stream_helper.h"

namespace wt {

namespace {

const int kEpollFlags = EPOLLIN | EPOLLOUT | EPOLLET;
const char kSourceAddressTokenSecret[] = "secret";

}  // namespace

const size_t kNumSessionsToCreatePerSocketEvent = 16;

Http3Server::Http3Server(std::unique_ptr<quic::ProofSource> proof_source,
                         Http3ServerBackend* http3_server_backend)
    : Http3Server(std::move(proof_source),
                  http3_server_backend,
                  quic::AllSupportedVersions()) {}

Http3Server::Http3Server(
    std::unique_ptr<quic::ProofSource> proof_source,
    Http3ServerBackend* http3_server_backend,
    const quic::ParsedQuicVersionVector& supported_versions)
    : Http3Server(std::move(proof_source),
                  quic::QuicConfig(),
                  quic::QuicCryptoServerConfig::ConfigOptions(),
                  supported_versions,
                  http3_server_backend,
                  quic::kQuicDefaultConnectionIdLength) {}

Http3Server::Http3Server(
    std::unique_ptr<quic::ProofSource> proof_source,
    const quic::QuicConfig& config,
    const quic::QuicCryptoServerConfig::ConfigOptions& crypto_config_options,
    const quic::ParsedQuicVersionVector& supported_versions,
    Http3ServerBackend* http3_server_backend,
    uint8_t expected_server_connection_id_length)
    : port_(0),
      fd_(-1),
      packets_dropped_(0),
      overflow_supported_(false),
      silent_close_(false),
      config_(config),
      crypto_config_(kSourceAddressTokenSecret,
                     quic::QuicRandom::GetInstance(),
                     std::move(proof_source),
                     quic::KeyExchangeSource::Default()),
      crypto_config_options_(crypto_config_options),
      version_manager_(supported_versions),
      packet_reader_(new quic::QuicPacketReader()),
      http3_server_backend_(http3_server_backend),
      expected_server_connection_id_length_(
          expected_server_connection_id_length) {
  DCHECK(http3_server_backend_);
  Initialize();
}

void Http3Server::Initialize() {
  // If an initial flow control window has not explicitly been set, then use a
  // sensible value for a server: 1 MB for session, 64 KB for each stream.
  const uint32_t kInitialSessionFlowControlWindow = 1 * 1024 * 1024;  // 1 MB
  const uint32_t kInitialStreamFlowControlWindow = 64 * 1024;         // 64 KB
  if (config_.GetInitialStreamFlowControlWindowToSend() ==
      quic::kDefaultFlowControlSendWindow) {
    config_.SetInitialStreamFlowControlWindowToSend(
        kInitialStreamFlowControlWindow);
  }
  if (config_.GetInitialSessionFlowControlWindowToSend() ==
      quic::kDefaultFlowControlSendWindow) {
    config_.SetInitialSessionFlowControlWindowToSend(
        kInitialSessionFlowControlWindow);
  }

  epoll_server_.set_timeout_in_us(50 * 1000);

  quic::QuicEpollClock clock(&epoll_server_);
}

Http3Server::~Http3Server() = default;

bool Http3Server::CreateUDPSocketAndListen(
    const quic::QuicSocketAddress& address) {
  quic::QuicUdpSocketApi socket_api;
  fd_ = socket_api.Create(
      address.host().AddressFamilyToInt(),
      /*receive_buffer_size =*/quic::kDefaultSocketReceiveBuffer,
      /*send_buffer_size =*/quic::kDefaultSocketReceiveBuffer);
  if (fd_ == quic::kQuicInvalidSocketFd) {
    PLOG(ERROR) << "CreateSocket() failed. address=" << address;
    return false;
  }
  overflow_supported_ = socket_api.EnableDroppedPacketCount(fd_);
  socket_api.EnableReceiveTimestamp(fd_);

  if (!socket_api.Bind(fd_, address)) {
    PLOG(ERROR) << "Bind failed. address=" << address;
    return false;
  }

  LOG(INFO) << "Listening on " << address.ToString();
  port_ = address.port();
  if (port_ == 0) {
    quic::QuicSocketAddress tmp_address;
    if (tmp_address.FromSocket(fd_) != 0) {
      PLOG(ERROR) << "Unable to get self address.";
      return false;
    }
    port_ = tmp_address.port();
  }

  epoll_server_.RegisterFD(fd_, this, kEpollFlags);
  dispatcher_.reset(CreateDispatcher());
  dispatcher_->InitializeWithWriter(CreateWriter(fd_));

  return true;
}

quic::QuicPacketWriter* Http3Server::CreateWriter(int fd) {
  return new quic::QuicDefaultPacketWriter(fd);
}

Http3ServerDispatcher* Http3Server::CreateDispatcher() {
  return new Http3ServerDispatcher(
      &config_, &crypto_config_, &version_manager_,
      std::make_unique<quic::QuicEpollConnectionHelper>(
          &epoll_server_, quic::QuicAllocator::BUFFER_POOL),
      std::unique_ptr<quic::QuicCryptoServerStreamBase::Helper>(
          new quic::QuicSimpleCryptoServerStreamHelper()),
      std::make_unique<quic::QuicEpollAlarmFactory>(&epoll_server_),
      http3_server_backend_, expected_server_connection_id_length_);
}

[[noreturn]] void Http3Server::HandleEventsForever() {
  while (true) {
    WaitForEvents();
  }
}

void Http3Server::WaitForEvents() {
  epoll_server_.WaitForEventsAndExecuteCallbacks();
}

void Http3Server::Shutdown() {
  if (!silent_close_) {
    // Before we shut down the epoll server, give all active sessions a chance
    // to notify clients that they're closing.
    dispatcher_->Shutdown();
  }

  epoll_server_.Shutdown();

  close(fd_);
  fd_ = -1;
}

void Http3Server::OnEvent(int fd, quic::QuicEpollEvent* event) {
  DCHECK_EQ(fd, fd_);
  event->out_ready_mask = 0;

  if (event->in_events & EPOLLIN) {
    dispatcher_->ProcessBufferedChlos(kNumSessionsToCreatePerSocketEvent);

    bool more_to_read = true;
    while (more_to_read) {
      more_to_read = packet_reader_->ReadAndDispatchPackets(
          fd_, port_, quic::QuicEpollClock(&epoll_server_), dispatcher_.get(),
          overflow_supported_ ? &packets_dropped_ : nullptr);
    }

    if (dispatcher_->HasChlosBuffered()) {
      // Register EPOLLIN event to consume buffered CHLO(s).
      event->out_ready_mask |= EPOLLIN;
    }
  }

  if (event->in_events & EPOLLOUT) {
    dispatcher_->OnCanWrite();
    if (dispatcher_->HasPendingWrites()) {
      event->out_ready_mask |= EPOLLOUT;
    }
  }
}

}  // namespace wt
