#include "web_transport/http3_backend_response.h"

namespace wt {

Http3BackendResponse::ServerPushInfo::ServerPushInfo(
    quic::QuicUrl request_url,
    spdy::Http2HeaderBlock headers,
    spdy::SpdyPriority priority,
    std::string body)
    : request_url(request_url),
      headers(std::move(headers)),
      priority(priority),
      body(body) {}

Http3BackendResponse::ServerPushInfo::ServerPushInfo(
    const Http3BackendResponse::ServerPushInfo& other)
    : request_url(other.request_url),
      headers(other.headers.Clone()),
      priority(other.priority),
      body(other.body) {}

Http3BackendResponse::Http3BackendResponse()
    : response_type_(REGULAR_RESPONSE) {}

void Http3BackendResponse::AddEarlyHints(
    const spdy::Http2HeaderBlock& headers) {
  spdy::Http2HeaderBlock hints = headers.Clone();
  hints[":status"] = "103";
  early_hints_.push_back(std::move(hints));
}

Http3BackendResponse::~Http3BackendResponse() = default;

}  // namespace wt