#include <csignal>
#include <fstream>

#include "absl/flags/flag.h"
#include "absl/flags/parse.h"
#include "absl/flags/usage.h"
#include "quiche/quic/core/crypto/proof_source_x509.h"
#include "quiche/quic/platform/api/quic_socket_address.h"
#include "web_transport/http3_server.h"
#include "web_transport/web_transport_server_backend.h"

// TODO(okka-riswana): use quic::QuicIpAddress directly as flag
ABSL_FLAG(std::string,
          address,
          "::1",
          "The IP address the quic server will listen on.");
ABSL_FLAG(uint16_t, port, 4433, "The port the quic server will listen on.");
ABSL_FLAG(std::string, cert_file, "", "The path to the certificate file.");
ABSL_FLAG(std::string, key_file, "", "The path to the key file.");

void GlogPrefix(std::ostream& s, const google::LogMessageInfo& l, void*) {
  // clang-format off
  s << '['
    << std::setw(2) << 1 + l.time.month()
    << std::setw(2) << l.time.day()
    << '/'
    << std::setw(2) << l.time.hour()
    << std::setw(2) << l.time.min()
    << std::setw(2) << l.time.sec() << '.'
    << std::setw(6) << l.time.usec()
    << ':'
    << l.severity
    << ':'
    << l.filename << '(' << l.line_number << ')'
    << ']';
  // clang-format on
}

namespace {

std::function<void(int sig)> shutdown_handler = nullptr;

void signal_handler(int sig) {
  if (shutdown_handler) {
    shutdown_handler(sig);
  }
}

}  // namespace

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0], &GlogPrefix);
  google::InstallFailureSignalHandler();
  absl::ParseCommandLine(argc, argv);

  std::signal(SIGTERM, signal_handler);
  std::signal(SIGINT, signal_handler);

  auto cert_file = std::ifstream(absl::GetFlag(FLAGS_cert_file));
  if (!cert_file.good()) {
    LOG(FATAL) << "could not open cert file";
  }
  auto key_file = std::ifstream(absl::GetFlag(FLAGS_key_file));
  if (!key_file.good()) {
    LOG(FATAL) << "could not open key file";
  }
  auto cert = quic::CertificateView::LoadPemFromStream(&cert_file);
  auto private_key = quic::CertificatePrivateKey::LoadPemFromStream(&key_file);
  auto chain =
      quiche::QuicheReferenceCountedPointer(new quic::ProofSource::Chain(cert));
  auto proof_source =
      quic::ProofSourceX509::Create(std::move(chain), std::move(*private_key));

  const auto& ip_str = absl::GetFlag(FLAGS_address);
  quic::QuicIpAddress ip;
  if (!ip.FromString(ip_str)) {
    LOG(FATAL) << "invalid host IP address: " << ip_str;
  }
  auto address = quic::QuicSocketAddress(ip, absl::GetFlag(FLAGS_port));

  wt::WebTransportServerBackend backend;
  auto server = wt::Http3Server(std::move(proof_source), &backend);
  if (!server.CreateUDPSocketAndListen(address)) {
    LOG(FATAL) << "could not create UDP socket";
  }

  shutdown_handler = [&server](int sig) {
    LOG(INFO) << "Got signal: " << strsignal(sig) << ". Shutting down server";
    server.Shutdown();
    std::exit(0);
  };

  server.HandleEventsForever();
}
