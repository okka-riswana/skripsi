#ifndef IMPL_CPP_QUICHE_WEB_TRANSPORT_LENGTH_PREFIXED_CODEC_H_
#define IMPL_CPP_QUICHE_WEB_TRANSPORT_LENGTH_PREFIXED_CODEC_H_

#include "absl/base/internal/endian.h"
#include "absl/strings/string_view.h"
#include "quiche/common/quiche_circular_deque.h"

namespace wt {

class LengthPrefixedEncoder {
 public:
  bool Encode(const absl::string_view message, std::string* out) {
    if (out == nullptr) {
      return false;
    }

    const uint64_t size = message.size();
    int varint_length;
    uint8_t prefix;

    if (size <= 63) {
      out->insert(out->end(), static_cast<char>(size));
      out->insert(out->end(), message.cbegin(), message.cend());
      return true;
    } else if (size <= 16383) {
      varint_length = 2;
      prefix = 0x40;
    } else if (size <= 1073741823) {
      varint_length = 4;
      prefix = 0x80;
    } else if (size <= 4611686018427387903) {
      varint_length = 8;
      prefix = 0xc0;
    } else {
      LOG(ERROR) << "Message is too large to encode";
      return false;
    }

    const auto size_bytes =
        absl::bit_cast<std::array<char, 8>>(absl::ghtonll(size));
    auto msb = out->insert(out->end(), size_bytes.cend() - varint_length,
                           size_bytes.cend());
    *msb |= prefix;
    out->insert(out->end(), message.cbegin(), message.cend());
    return true;
  }

  std::string Encode(const absl::string_view message) {
    std::string encoded;
    Encode(message, &encoded);
    return encoded;
  }
};

class LengthPrefixedDecoder {
 public:
  LengthPrefixedDecoder() = default;
  LengthPrefixedDecoder(const LengthPrefixedDecoder&) = delete;
  LengthPrefixedDecoder(LengthPrefixedDecoder&&) = delete;

  std::string PullMessage() {
    DCHECK(!messages_.empty());
    const auto& [varint_length, message_length] = messages_.front();
    const size_t total_length = varint_length + message_length;
    const std::string message(buffer_.cbegin() + varint_length,
                              buffer_.cbegin() + total_length);
    buffer_.erase(0, total_length);
    messages_.pop_front();
    return message;
  }

  bool TryParseMessage() {
    if (current_msg_length_ == 0 && !TryParseLength()) {
      return false;
    }

    if (buffer_.size() < current_varint_length_ + current_msg_length_) {
      return false;
    }

    messages_.emplace_back(current_varint_length_, current_msg_length_);
    current_varint_length_ = 0;
    current_msg_length_ = 0;

    return true;
  }

  bool TryParseLength() {
    if (buffer_.empty()) {
      return false;
    }

    uint64_t v = static_cast<uint8_t>(buffer_[0]);
    const int length = 1 << (v >> 6);

    if (buffer_.size() < length) {
      return false;
    }

    v &= 0x3f;
    for (int i = 0; i < length - 1; ++i) {
      v = (v << 8) + static_cast<uint8_t>(buffer_[i + 1]);
    }

    current_varint_length_ = length;
    current_msg_length_ = v;
    return true;
  }

  size_t BufferedMessages() const { return messages_.size(); }

  std::string* buffer() { return &buffer_; }
  const std::string* buffer() const { return &buffer_; }

 private:
  int current_varint_length_ = 0;
  uint64_t current_msg_length_ = 0;
  std::string buffer_;
  // varint_length and message_length pairs
  quiche::QuicheCircularDeque<std::pair<int, size_t>> messages_;
};

}  // namespace wt

#endif  // IMPL_CPP_QUICHE_WEB_TRANSPORT_LENGTH_PREFIXED_CODEC_H_
