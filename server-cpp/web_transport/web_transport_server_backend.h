#ifndef IMPL_CPP_QUICHE_WEB_TRANSPORT_WEB_TRANSPORT_SERVER_BACKEND_H_
#define IMPL_CPP_QUICHE_WEB_TRANSPORT_WEB_TRANSPORT_SERVER_BACKEND_H_

#include "web_transport/http3_server_backend.h"

namespace wt {

class WebTransportServerBackend : public Http3ServerBackend {
 public:
  WebTransportServerBackend() = default;

  // From Http3ServerBackend

  bool InitializeBackend(const std::string&) override { return true; }
  bool IsBackendInitialized() const override { return true; }
  void FetchResponseFromBackend(const spdy::Http2HeaderBlock& request_headers,
                                const std::string& request_body,
                                RequestHandler* request_handler) override {}
  void CloseBackendResponseStream(RequestHandler* request_handler) override {}
  WebTransportResponse ProcessWebTransportRequest(
      const spdy::Http2HeaderBlock& request_headers,
      quic::WebTransportSession* session) override;
  bool SupportsWebTransport() override { return true; }
};

}  // namespace wt

#endif  // IMPL_CPP_QUICHE_WEB_TRANSPORT_WEB_TRANSPORT_SERVER_BACKEND_H_
