#include "web_transport/http3_dispatcher.h"
#include "web_transport/http3_server_session.h"

namespace wt {

Http3ServerDispatcher::Http3ServerDispatcher(
    const quic::QuicConfig* config,
    const quic::QuicCryptoServerConfig* crypto_config,
    quic::QuicVersionManager* version_manager,
    std::unique_ptr<quic::QuicConnectionHelperInterface> helper,
    std::unique_ptr<quic::QuicCryptoServerStreamBase::Helper> session_helper,
    std::unique_ptr<quic::QuicAlarmFactory> alarm_factory,
    Http3ServerBackend* http3_server_backend,
    uint8_t expected_server_connection_id_length)
    : QuicDispatcher(config,
                     crypto_config,
                     version_manager,
                     std::move(helper),
                     std::move(session_helper),
                     std::move(alarm_factory),
                     expected_server_connection_id_length),
      http3_server_backend_(http3_server_backend) {}

Http3ServerDispatcher::~Http3ServerDispatcher() = default;

int Http3ServerDispatcher::GetRstErrorCount(
    quic::QuicRstStreamErrorCode error_code) const {
  auto it = rst_error_map_.find(error_code);
  if (it == rst_error_map_.end()) {
    return 0;
  }
  return it->second;
}

void Http3ServerDispatcher::OnRstStreamReceived(
    const quic::QuicRstStreamFrame& frame) {
  auto it = rst_error_map_.find(frame.error_code);
  if (it == rst_error_map_.end()) {
    rst_error_map_.insert(std::make_pair(frame.error_code, 1));
  } else {
    it->second++;
  }
}

std::unique_ptr<quic::QuicSession> Http3ServerDispatcher::CreateQuicSession(
    quic::QuicConnectionId connection_id,
    const quic::QuicSocketAddress& self_address,
    const quic::QuicSocketAddress& peer_address,
    absl::string_view /*alpn*/,
    const quic::ParsedQuicVersion& version,
    const quic::ParsedClientHello& /*parsed_chlo*/) {
  // The QuicServerSessionBase takes ownership of |connection| below.
  quic::QuicConnection* connection = new quic::QuicConnection(
      connection_id, self_address, peer_address, helper(), alarm_factory(),
      writer(),
      /* owns_writer= */ false, quic::Perspective::IS_SERVER,
      quic::ParsedQuicVersionVector{version});

  auto session = std::make_unique<Http3ServerSession>(
      config(), GetSupportedVersions(), connection, this, session_helper(),
      crypto_config(), compressed_certs_cache(), http3_server_backend_);
  session->Initialize();
  return session;
}

}  // namespace wt
