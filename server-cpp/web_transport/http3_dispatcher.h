#ifndef IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_DISPATCHER_H_
#define IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_DISPATCHER_H_

#include "quiche/quic/core/quic_dispatcher.h"
#include "web_transport/http3_server_backend.h"

namespace wt {

class Http3ServerDispatcher : public quic::QuicDispatcher {
 public:
  Http3ServerDispatcher(
      const quic::QuicConfig* config,
      const quic::QuicCryptoServerConfig* crypto_config,
      quic::QuicVersionManager* version_manager,
      std::unique_ptr<quic::QuicConnectionHelperInterface> helper,
      std::unique_ptr<quic::QuicCryptoServerStreamBase::Helper> session_helper,
      std::unique_ptr<quic::QuicAlarmFactory> alarm_factory,
      Http3ServerBackend* http3_server_backend,
      uint8_t expected_server_connection_id_length);

  ~Http3ServerDispatcher() override;

  int GetRstErrorCount(quic::QuicRstStreamErrorCode rst_error_code) const;

  void OnRstStreamReceived(const quic::QuicRstStreamFrame& frame) override;

 protected:
  std::unique_ptr<quic::QuicSession> CreateQuicSession(
      quic::QuicConnectionId connection_id,
      const quic::QuicSocketAddress& self_address,
      const quic::QuicSocketAddress& peer_address,
      absl::string_view alpn,
      const quic::ParsedQuicVersion& version,
      const quic::ParsedClientHello& parsed_chlo) override;

  Http3ServerBackend* server_backend() { return http3_server_backend_; }

 private:
  Http3ServerBackend* http3_server_backend_;  // Unowned.

  // The map of the reset error code with its counter.
  std::map<quic::QuicRstStreamErrorCode, int> rst_error_map_;
};
}  // namespace wt

#endif  // IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_DISPATCHER_H_
