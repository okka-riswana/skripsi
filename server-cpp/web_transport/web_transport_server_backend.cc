#include "web_transport/web_transport_server_backend.h"
#include "web_transport/web_transport_echo_visitors.h"

namespace wt {

Http3ServerBackend::WebTransportResponse
WebTransportServerBackend::ProcessWebTransportRequest(
    const spdy::Http2HeaderBlock& request_headers,
    quic::WebTransportSession* session) {
  auto path_it = request_headers.find(":path");
  if (path_it == request_headers.end()) {
    WebTransportResponse response;
    response.response_headers[":status"] = "400";
    return response;
  }
  WebTransportResponse response;
  absl::string_view path = path_it->second;
  if (path == "/echo") {
    response.response_headers[":status"] = "200";
    response.visitor =
        std::make_unique<EchoWebTransportSessionVisitor>(session);
    return response;
  }

  response.response_headers[":status"] = "404";
  return response;
}

}  // namespace wt
