#ifndef IMPL_CPP_QUICHE_WEB_TRANSPORT_WEB_TRANSPORT_ECHO_VISITORS_H_
#define IMPL_CPP_QUICHE_WEB_TRANSPORT_WEB_TRANSPORT_ECHO_VISITORS_H_

#include "quiche/common/platform/api/quiche_mem_slice.h"
#include "quiche/common/simple_buffer_allocator.h"
#include "quiche/quic/core/web_transport_interface.h"
#include "web_transport/length_prefixed_codec.h"

namespace wt {

// Echoes any incoming data back on the same stream.
class WebTransportBidirectionalEchoVisitor
    : public quic::WebTransportStreamVisitor {
 public:
  explicit WebTransportBidirectionalEchoVisitor(
      quic::WebTransportStream* stream)
      : stream_(stream) {}

  void OnCanRead() override {
    auto result = stream_->Read(decoder_.buffer());
    if (result.bytes_read > 0) {
      while (decoder_.TryParseMessage()) {
        TryWrite(decoder_.PullMessage());
      }
    }
    if (result.fin && stream_->CanWrite()) {
      bool fin_sent = stream_->SendFin();
      DCHECK(fin_sent);
    }
  }

  void OnCanWrite() override {}

  void OnResetStreamReceived(quic::WebTransportStreamError /*error*/) override {
    stream_->ResetWithUserCode(quic::QUIC_RST_ACKNOWLEDGEMENT);
  }

  void OnStopSendingReceived(quic::WebTransportStreamError /*error*/) override {
    stream_->ResetWithUserCode(quic::QUIC_NO_ERROR);
  }

  void OnWriteSideInDataRecvdState() override {}

 protected:
  quic::WebTransportStream* stream() { return stream_; }

 private:
  bool TryWrite(absl::string_view message) {
    if (!stream_->CanWrite() || message.empty()) {
      return false;
    }
    if (!encoder_.Encode(message, &write_buffer_)) {
      return false;
    }
    bool success = stream_->Write(write_buffer_);
    write_buffer_.clear();
    return success;
  }

  quic::WebTransportStream* stream_;
  std::string write_buffer_;
  LengthPrefixedDecoder decoder_;
  LengthPrefixedEncoder encoder_;
};

// Buffers all the data and calls |callback| with the entirety of the stream
// data.
class WebTransportUnidirectionalEchoReadVisitor
    : public quic::WebTransportStreamVisitor {
 public:
  WebTransportUnidirectionalEchoReadVisitor(
      quic::WebTransportStream* stream,
      quic::WebTransportStream* write_stream)
      : stream_(stream), write_stream_(write_stream) {}

  void OnCanRead() override {
    auto result = stream_->Read(decoder_.buffer());
    if (result.bytes_read > 0) {
      while (decoder_.TryParseMessage()) {
        TryWrite(decoder_.PullMessage());
      }
    }
    if (result.fin && write_stream_->CanWrite()) {
      bool fin_sent = write_stream_->SendFin();
      DCHECK(fin_sent);
    }
  }

  void OnCanWrite() override { LOG(FATAL) << "unreachable"; }

  void OnResetStreamReceived(quic::WebTransportStreamError /*error*/) override {
  }
  void OnStopSendingReceived(quic::WebTransportStreamError /*error*/) override {
  }
  void OnWriteSideInDataRecvdState() override {}

 private:
  bool TryWrite(absl::string_view message) {
    if (!write_stream_->CanWrite() || message.empty()) {
      return false;
    }
    if (!encoder_.Encode(message, &write_buffer_)) {
      return false;
    }
    bool success = write_stream_->Write(write_buffer_);
    write_buffer_.clear();
    return success;
  }


  quic::WebTransportStream* stream_;        // Not owned.
  quic::WebTransportStream* write_stream_;  // Not owned.
  std::string write_buffer_;
  LengthPrefixedDecoder decoder_;
  LengthPrefixedEncoder encoder_;
};

// A EchoSession visitor which sets unidirectional or bidirectional stream visitors
// to echo.
class EchoWebTransportSessionVisitor : public quic::WebTransportVisitor {
 public:
  explicit EchoWebTransportSessionVisitor(quic::WebTransportSession* session)
      : session_(session) {}

  void OnSessionReady(const spdy::SpdyHeaderBlock&) override {}

  void OnSessionClosed(quic::WebTransportSessionError /*error_code*/,
                       const std::string& /*error_message*/) override {}

  void OnIncomingBidirectionalStreamAvailable() override {
    while (true) {
      quic::WebTransportStream* stream =
          session_->AcceptIncomingBidirectionalStream();
      if (stream == nullptr) {
        return;
      }
      stream->SetVisitor(
          std::make_unique<WebTransportBidirectionalEchoVisitor>(stream));
    }
  }

  void OnIncomingUnidirectionalStreamAvailable() override {
    while (true) {
      quic::WebTransportStream* stream =
          session_->AcceptIncomingUnidirectionalStream();
      if (stream == nullptr) {
        return;
      }
      quic::WebTransportStream* write_stream =
          session_->OpenOutgoingUnidirectionalStream();
      if (write_stream == nullptr) {
        LOG(ERROR) << "Failed to open outgoing unidirectional stream";
        stream->ResetWithUserCode(quic::QUIC_INTERNAL_ERROR);
        return;
      }
      stream->SetVisitor(
          std::make_unique<WebTransportUnidirectionalEchoReadVisitor>(
              stream, write_stream));
    }
  }

  void OnDatagramReceived(absl::string_view datagram) override {
    quiche::QuicheMemSlice slice(
        quiche::QuicheBuffer::Copy(&allocator_, datagram));
    session_->SendOrQueueDatagram(std::move(slice));
  }

  void OnCanCreateNewOutgoingBidirectionalStream() override {}
  void OnCanCreateNewOutgoingUnidirectionalStream() override {}

 private:
  quic::WebTransportSession* session_;
  quiche::SimpleBufferAllocator allocator_;
};

}  // namespace wt

#endif  // IMPL_CPP_QUICHE_WEB_TRANSPORT_WEB_TRANSPORT_ECHO_VISITORS_H_
