#ifndef IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_H_
#define IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_H_

#include "quiche/quic/core/crypto/proof_source.h"
#include "quiche/quic/core/crypto/quic_crypto_server_config.h"
#include "quiche/quic/core/quic_config.h"
#include "quiche/quic/core/quic_dispatcher.h"
#include "quiche/quic/core/quic_epoll_clock.h"
#include "quiche/quic/core/quic_packet_reader.h"
#include "quiche/quic/core/quic_udp_socket.h"
#include "quiche/quic/platform/api/quic_epoll.h"
#include "web_transport/http3_dispatcher.h"
#include "web_transport/http3_server_backend.h"

namespace wt {

class Http3Server : public quic::QuicEpollCallbackInterface {
 public:
  Http3Server(std::unique_ptr<quic::ProofSource> proof_source,
              Http3ServerBackend* http3_server_backend);
  Http3Server(std::unique_ptr<quic::ProofSource> proof_source,
              Http3ServerBackend* http3_server_backend,
              const quic::ParsedQuicVersionVector& supported_versions);
  Http3Server(
      std::unique_ptr<quic::ProofSource> proof_source,
      const quic::QuicConfig& config,
      const quic::QuicCryptoServerConfig::ConfigOptions& crypto_config_options,
      const quic::ParsedQuicVersionVector& supported_versions,
      Http3ServerBackend* http3_server_backend,
      uint8_t expected_server_connection_id_length);
  Http3Server(const Http3Server&) = delete;
  Http3Server& operator=(const Http3Server&) = delete;

  ~Http3Server() override;

  std::string Name() const override { return "Http3 Server"; }

  // Start listening on the specified address.
  bool CreateUDPSocketAndListen(const quic::QuicSocketAddress& address);
  // Handles all events. Does not return.
  [[noreturn]] void HandleEventsForever();

  // Wait up to 50ms, and handle any events which occur.
  void WaitForEvents();

  // Server deletion is imminent.  Start cleaning up the epoll server.
  void Shutdown();

  // From quic::EpollCallbackInterface
  void OnRegistration(quic::QuicEpollServer* /*eps*/,
                      int /*fd*/,
                      int /*event_mask*/) override {}
  void OnModification(int /*fd*/, int /*event_mask*/) override {}
  void OnEvent(int /*fd*/, quic::QuicEpollEvent* /*event*/) override;
  void OnUnregistration(int /*fd*/, bool /*replaced*/) override {}
  void OnShutdown(quic::QuicEpollServer* /*eps*/, int /*fd*/) override {}

  void SetChloMultiplier(size_t multiplier) {
    crypto_config_.set_chlo_multiplier(multiplier);
  }

  void SetPreSharedKey(absl::string_view key) {
    crypto_config_.set_pre_shared_key(key);
  }

  bool overflow_supported() { return overflow_supported_; }

  quic::QuicPacketCount packets_dropped() { return packets_dropped_; }

  int port() { return port_; }

  quic::QuicEpollServer* epoll_server() { return &epoll_server_; }

 protected:
  quic::QuicPacketWriter* CreateWriter(int fd);

  Http3ServerDispatcher* CreateDispatcher();

  const quic::QuicConfig& config() const { return config_; }
  const quic::QuicCryptoServerConfig& crypto_config() const {
    return crypto_config_;
  }

  quic::QuicDispatcher* dispatcher() { return dispatcher_.get(); }

  quic::QuicVersionManager* version_manager() { return &version_manager_; }

  Http3ServerBackend* server_backend() { return http3_server_backend_; }

  void set_silent_close(bool value) { silent_close_ = value; }

  uint8_t expected_server_connection_id_length() {
    return expected_server_connection_id_length_;
  }

 private:
  // Initialize the internal state of the server.
  void Initialize();

  // Accepts data from the framer and demuxes clients to sessions.
  std::unique_ptr<quic::QuicDispatcher> dispatcher_;
  // Frames incoming packets and hands them to the dispatcher.
  quic::QuicEpollServer epoll_server_;

  // The port the server is listening on.
  int port_;

  // Listening connection.  Also used for outbound client communication.
  quic::QuicUdpSocketFd fd_;

  // If overflow_supported_ is true this will be the number of packets dropped
  // during the lifetime of the server.  This may overflow if enough packets
  // are dropped.
  quic::QuicPacketCount packets_dropped_;

  // True if the kernel supports SO_RXQ_OVFL, the number of packets dropped
  // because the socket would otherwise overflow.
  bool overflow_supported_;

  // If true, do not call Shutdown on the dispatcher.  Connections will close
  // without sending a final connection close.
  bool silent_close_;

  // config_ contains non-crypto parameters that are negotiated in the crypto
  // handshake.
  quic::QuicConfig config_;
  // crypto_config_ contains crypto parameters for the handshake.
  quic::QuicCryptoServerConfig crypto_config_;
  // crypto_config_options_ contains crypto parameters for the handshake.
  quic::QuicCryptoServerConfig::ConfigOptions crypto_config_options_;

  // Used to generate current supported versions.
  quic::QuicVersionManager version_manager_;

  // Point to a QuicPacketReader object on the heap. The reader allocates more
  // space than allowed on the stack.
  std::unique_ptr<quic::QuicPacketReader> packet_reader_;

  Http3ServerBackend* http3_server_backend_;  // unowned.

  // Connection ID length expected to be read on incoming IETF short headers.
  uint8_t expected_server_connection_id_length_;
};

}  // namespace wt

#endif  // IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_H_
