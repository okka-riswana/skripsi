#ifndef IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_SESSION_H_
#define IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_SESSION_H_

#include "quiche/quic/core/http/quic_server_session_base.h"
#include "web_transport/http3_server_backend.h"

namespace wt {

class Http3ServerSession : public quic::QuicServerSessionBase {
 public:
  // A PromisedStreamInfo is an element of the queue to store promised
  // stream which hasn't been created yet. It keeps a mapping between promised
  // stream id with its priority and the headers sent out in PUSH_PROMISE.
  struct PromisedStreamInfo {
   public:
    PromisedStreamInfo(spdy::Http2HeaderBlock request_headers,
                       quic::QuicStreamId stream_id,
                       const spdy::SpdyStreamPrecedence& precedence)
        : request_headers(std::move(request_headers)),
          stream_id(stream_id),
          precedence(precedence) {}
    spdy::Http2HeaderBlock request_headers;
    quic::QuicStreamId stream_id;
    spdy::SpdyStreamPrecedence precedence;
    bool is_cancelled = false;
  };

  // Takes ownership of |connection|.
  explicit Http3ServerSession(
      const ::quic::QuicConfig& config,
      const ::quic::ParsedQuicVersionVector& supported_versions,
      ::quic::QuicConnection* connection,
      ::quic::QuicSession::Visitor* visitor,
      ::quic::QuicCryptoServerStreamBase::Helper* helper,
      const ::quic::QuicCryptoServerConfig* crypto_config,
      ::quic::QuicCompressedCertsCache* compressed_certs_cache,
      Http3ServerBackend* http3_server_backend);
  Http3ServerSession& operator=(Http3ServerSession&) = delete;

  ~Http3ServerSession() override;

  // Override base class to detact client sending data on server push stream.
  void OnStreamFrame(const quic::QuicStreamFrame& frame) override;

  void OnCanCreateNewOutgoingStream(bool unidirectional) override;

 protected:
  // From quic::QuicSession
  quic::QuicSpdyStream* CreateIncomingStream(quic::QuicStreamId id) override;
  quic::QuicSpdyStream* CreateIncomingStream(
      quic::PendingStream* pending) override;
  quic::QuicSpdyStream* CreateOutgoingBidirectionalStream() override;
  quic::QuicSpdyStream* CreateOutgoingUnidirectionalStream() override;
  // Override to return true for locally preserved server push stream.
  void HandleFrameOnNonexistentOutgoingStream(
      quic::QuicStreamId stream_id) override;
  // Override to handle reseting locally preserved streams.
  void HandleRstOnValidNonexistentStream(
      const quic::QuicRstStreamFrame& frame) override;

  // From quic::QuicServerSessionBase
  std::unique_ptr<quic::QuicCryptoServerStreamBase>
  CreateQuicCryptoServerStream(
      const quic::QuicCryptoServerConfig* crypto_config,
      quic::QuicCompressedCertsCache* compressed_certs_cache) override;

  void MaybeInitializeHttp3UnidirectionalStreams() override;

  bool ShouldNegotiateWebTransport() override {
    return http3_server_backend_->SupportsWebTransport();
  }

  quic::HttpDatagramSupport LocalHttpDatagramSupport() override {
    if (ShouldNegotiateWebTransport()) {
      return quic::HttpDatagramSupport::kDraft04And09;
    }
    return QuicServerSessionBase::LocalHttpDatagramSupport();
  }

  Http3ServerBackend* server_backend() { return http3_server_backend_; }

 private:
  // Fetch response from cache for request headers enqueued into
  // promised_headers_and_streams_ and send them on dedicated stream until
  // reaches max_open_stream_ limit.
  // Called when return value of GetNumOpenOutgoingStreams() changes:
  //    CloseStreamInner();
  //    StreamDraining();
  // Note that updateFlowControlOnFinalReceivedByteOffset() won't change the
  // return value becasue all push streams are impossible to become locally
  // closed. Since a locally preserved stream becomes remotely closed after
  // HandlePromisedPushRequests() starts to process it, and if it is reset
  // locally afterwards, it will be immediately become closed and never get into
  // locally_closed_stream_highest_offset_. So all the streams in this map
  // are not outgoing streams.
  void HandlePromisedPushRequests();

  // Promised streams which hasn't been created yet because of max_open_stream_
  // limit. New element is added to the end of the queue.
  // Since outgoing stream is created in sequence, stream_id of each element in
  // the queue also increases by 2 from previous one's. The front element's
  // stream_id is always next_outgoing_stream_id_, and the last one is always
  // highest_promised_stream_id_.
  quiche::QuicheCircularDeque<PromisedStreamInfo> promised_streams_;

  quic::QuicStreamId highest_promised_stream_id_;
  Http3ServerBackend* http3_server_backend_;
};

}  // namespace wt

#endif  // IMPL_CPP_QUICHE_WEB_TRANSPORT_HTTP3_SERVER_SESSION_H_
