#!/usr/bin/env bash

set -e;

# Must execute from the project root directory
cd "$(dirname "$0")/..";
PROJECT_DIR="$(pwd)";

try() {
  "$@" || (e=$?; echo "$@" > /dev/stderr; unset CA_DIR; exit $e)
}

export CA_DIR="$PROJECT_DIR/_certs";
out="$CA_DIR";
cert_conf_dir="$PROJECT_DIR/tools/certs";

try rm -rf "$out"
try mkdir -p "$out"

try /bin/sh -c "echo 01 > $out/2048-sha256-root-serial"
touch "$out/2048-sha256-root-index.txt"

# Generate the key.
try openssl genrsa -out "$out/2048-sha256-root.key" 2048

# Generate the root certificate.
try openssl req \
  -new \
  -key "$out/2048-sha256-root.key" \
  -out "$out/2048-sha256-root.req" \
  -config "$cert_conf_dir/ca.cnf"

try openssl x509 \
  -req -days 3 \
  -in "$out/2048-sha256-root.req" \
  -signkey "$out/2048-sha256-root.key" \
  -extfile "$cert_conf_dir/ca.cnf" \
  -extensions ca_cert \
  -text > "$out/2048-sha256-root.pem"

# Generate the leaf certificate request.
try openssl req \
  -new \
  -keyout "$out/leaf_cert.key" \
  -out "$out/leaf_cert.req" \
  -config "$cert_conf_dir/leaf.cnf"

# Convert the key to pkcs8.
try openssl pkcs8 \
  -topk8 \
  -outform DER \
  -inform PEM \
  -in "$out/leaf_cert.key" \
  -out "$out/leaf_cert.pkcs8" \
  -nocrypt

# Generate the leaf certificate to be valid for three days.
try openssl ca \
  -batch \
  -days 3 \
  -extensions user_cert \
  -in "$out/leaf_cert.req" \
  -out "$out/leaf_cert.pem" \
  -config "$cert_conf_dir/ca.cnf"

# Compute the fingerprint of the certificate.
try openssl x509 -pubkey -noout -in "$out/leaf_cert.pem" | \
  openssl rsa -pubin -outform der | \
  openssl dgst -sha256 -binary | base64 > "$out/fingerprint.txt"

unset CA_DIR;
