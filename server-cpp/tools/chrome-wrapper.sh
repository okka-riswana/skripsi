#!/usr/bin/env bash

set -e;

# Must execute from the project root directory
cd "$(dirname "$0")/..";
PROJECT_DIR="$(pwd)";

CHROME_BIN="${CHROME_BIN:-/usr/bin/google-chrome}";
QUIC_ORIGIN="${QUIC_PORT:-localhost:4433}";

# shellcheck disable=SC2068
"$CHROME_BIN" \
  --origin-to-force-quic-on="$QUIC_ORIGIN" \
  --ignore-certificate-errors-spki-list="$(cat "$PROJECT_DIR"/_certs/fingerprint.txt)" \
  $@ &>/dev/null &

disown -ar;
