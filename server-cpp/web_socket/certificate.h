#ifndef IMPL_CPP_QUICHE_WEB_SOCKET_CERTIFICATE_H_
#define IMPL_CPP_QUICHE_WEB_SOCKET_CERTIFICATE_H_

#include <fstream>

#include <boost/beast/websocket/ssl.hpp>

namespace {
namespace net = boost::asio;
namespace ssl = boost::asio::ssl;
}

namespace ws {

// TODO: Use command line arguments to specify the certificate and key files.
void load_server_certificate(ssl::context &ctx, const std::string &cert_path,
                             const std::string &key_path) {
  std::ifstream cert_file{cert_path};
  if (cert_file.bad()) {
    LOG(FATAL) << "unable to read certificate file: " << cert_path;
  }
  std::string cert{std::istreambuf_iterator<char>{cert_file},
                   std::istreambuf_iterator<char>{}};

  std::ifstream key_file{key_path};
  if (key_file.bad()) {
    LOG(FATAL) << "unable to read key file: " << key_path;
  }
  std::string key{std::istreambuf_iterator<char>{key_file},
                  std::istreambuf_iterator<char>{}};

  // This holds the self-signed certificate used by the server
  ctx.set_options(ssl::context::default_workarounds | ssl::context::no_sslv2);
  ctx.use_certificate_chain(net::buffer(cert));
  ctx.use_private_key(net::buffer(key), ssl::context::pem);
}

}

#endif  // IMPL_CPP_QUICHE_WEB_SOCKET_CERTIFICATE_H_
