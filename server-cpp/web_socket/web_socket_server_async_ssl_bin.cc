#include <boost/asio/dispatch.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/websocket/ssl.hpp>
#include <cstdlib>
#include <iomanip>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include "absl/flags/flag.h"
#include "absl/flags/parse.h"
#include "absl/flags/usage.h"
#include "glog/logging.h"
#include "web_socket/certificate.h"

namespace beast = boost::beast;
namespace http = beast::http;
namespace websocket = beast::websocket;
namespace net = boost::asio;
namespace ssl = boost::asio::ssl;
using tcp = boost::asio::ip::tcp;

// Report a failure
void HandleError(beast::error_code ec, char const* what) {
  if (ec == net::error::connection_reset ||
      ec == net::error::operation_aborted) {
    return;
  }
  LOG(ERROR) << what << ": " << ec.message();
}

// Echoes back all received WebSocket messages
class EchoSession : public std::enable_shared_from_this<EchoSession> {
  websocket::stream<beast::ssl_stream<beast::tcp_stream>> ws_;
  beast::flat_buffer buffer_;

 public:
  EchoSession(tcp::socket&& socket, ssl::context& ctx)
      : ws_(std::move(socket), ctx) {
    ws_.auto_fragment(false);
    ws_.read_message_max(64 * 1024 * 1024);
#ifdef ENABLE_PMD
    websocket::permessage_deflate pmd;
    pmd.client_enable = true;
    pmd.server_enable = true;
    pmd.compLevel = 3;
    ws_.set_option(pmd);
#endif
  }

  void Run() {
    net::dispatch(
        ws_.get_executor(),
        beast::bind_front_handler(&EchoSession::OnRun, shared_from_this()));
  }

  void OnRun() {
    beast::get_lowest_layer(ws_).expires_after(std::chrono::seconds(30));

    ws_.next_layer().async_handshake(
        ssl::stream_base::server,
        beast::bind_front_handler(&EchoSession::OnHandshake,
                                  shared_from_this()));
  }

  void OnHandshake(beast::error_code ec) {
    if (ec) {
      return HandleError(ec, "handshake");
    }

    // Turn off the timeout on the tcp_stream, because
    // the websocket stream has its own timeout system.
    beast::get_lowest_layer(ws_).expires_never();

    // Set suggested timeout settings for the websocket
    ws_.set_option(
        websocket::stream_base::timeout::suggested(beast::role_type::server));

    // Set a decorator to change the Server of the handshake
    ws_.set_option(
        websocket::stream_base::decorator([](websocket::response_type& res) {
          res.set(http::field::server, std::string(BOOST_BEAST_VERSION_STRING) +
                                           " websocket-server-async-ssl");
        }));

    // Accept the websocket handshake
    ws_.async_accept(
        beast::bind_front_handler(&EchoSession::OnAccept, shared_from_this()));
  }

  void OnAccept(beast::error_code ec) {
    if (ec) {
      return HandleError(ec, "accept");
    }
    DoRead();
  }

  void DoRead() {
    ws_.async_read(buffer_, beast::bind_front_handler(&EchoSession::OnRead,
                                                      shared_from_this()));
  }

  void OnRead(beast::error_code ec, std::size_t bytes_transferred) {
    boost::ignore_unused(bytes_transferred);

    // This indicates that the EchoSession was closed
    if (ec == websocket::error::closed) {
      return;
    }

    if (ec) {
      HandleError(ec, "read");
    }

    // Echo the message
    ws_.text(ws_.got_text());
    ws_.async_write(
        buffer_.data(),
        beast::bind_front_handler(&EchoSession::OnWrite, shared_from_this()));
  }

  void OnWrite(beast::error_code ec, std::size_t bytes_transferred) {
    boost::ignore_unused(bytes_transferred);

    if (ec) {
      return HandleError(ec, "write");
    }

    // Clear the buffer
    buffer_.consume(buffer_.size());

    // Do another read
    DoRead();
  }
};

//------------------------------------------------------------------------------

// Accepts incoming connections and launches the sessions
class EchoListener : public std::enable_shared_from_this<EchoListener> {
  net::io_context& ioc_;
  ssl::context& ctx_;
  tcp::acceptor acceptor_;

 public:
  EchoListener(net::io_context& ioc, ssl::context& ctx, tcp::endpoint endpoint)
      : ioc_(ioc), ctx_(ctx), acceptor_(net::make_strand(ioc)) {
    beast::error_code ec;

    acceptor_.open(endpoint.protocol(), ec);
    if (ec) {
      HandleError(ec, "open");
      return;
    }

    acceptor_.set_option(net::socket_base::reuse_address(true), ec);
    if (ec) {
      HandleError(ec, "set_option");
      return;
    }

    acceptor_.bind(endpoint, ec);
    if (ec) {
      HandleError(ec, "bind");
      return;
    }

    acceptor_.listen(net::socket_base::max_listen_connections, ec);
    if (ec) {
      HandleError(ec, "listen");
      return;
    }
  }

  void Run() { DoAccept(); }

 private:
  void DoAccept() {
    acceptor_.async_accept(
        net::make_strand(ioc_),
        beast::bind_front_handler(&EchoListener::OnAccept, shared_from_this()));
  }

  void OnAccept(beast::error_code ec, tcp::socket socket) {
    if (ec) {
      HandleError(ec, "accept");
    } else {
      std::make_shared<EchoSession>(std::move(socket), ctx_)->Run();
    }

    DoAccept();
  }
};

//------------------------------------------------------------------------------

ABSL_FLAG(std::string,
          address,
          "::1",
          "The IP address the websocket server will listen on.");
ABSL_FLAG(uint16_t,
          port,
          8080,
          "The port the websocket server will listen on.");
ABSL_FLAG(std::string, cert_file, "", "The path to the certificate file.");
ABSL_FLAG(std::string, key_file, "", "The path to the key file.");
ABSL_FLAG(int, threads, 1, "The number of threads to use.");

void GlogPrefix(std::ostream& s, const google::LogMessageInfo& l, void*) {
  // clang-format off
  s << '['
    << std::setw(2) << 1 + l.time.month()
    << std::setw(2) << l.time.day()
    << '/'
    << std::setw(2) << l.time.hour()
    << std::setw(2) << l.time.min()
    << std::setw(2) << l.time.sec() << '.'
    << std::setw(6) << l.time.usec()
    << ':'
    << l.severity
    << ':'
    << l.filename << '(' << l.line_number << ')'
    << ']';
  // clang-format on
}

namespace {

std::function<void(int sig)> shutdown_handler = nullptr;

void signal_handler(int sig) {
  if (shutdown_handler) {
    shutdown_handler(sig);
  }
}

}  // namespace

int main(int argc, char* argv[]) {
  google::InitGoogleLogging(argv[0], &GlogPrefix);
  google::InstallFailureSignalHandler();
  absl::ParseCommandLine(argc, argv);

  std::signal(SIGTERM, signal_handler);
  std::signal(SIGINT, signal_handler);

  const auto address = net::ip::make_address(absl::GetFlag(FLAGS_address));
  const auto port = absl::GetFlag(FLAGS_port);
  const auto threads = std::max<int>(1, absl::GetFlag(FLAGS_threads));

  // The io_context is required for all I/O
  net::io_context ioc{threads};

  // The SSL context is required, and holds certificates
  ssl::context ctx{ssl::context::tlsv13};

  // This holds the self-signed certificate used by the server
  ws::load_server_certificate(ctx, absl::GetFlag(FLAGS_cert_file),
                              absl::GetFlag(FLAGS_key_file));

  // Create and launch a listening port
  std::make_shared<EchoListener>(ioc, ctx, tcp::endpoint{address, port})->Run();

  // Run the I/O service on the requested number of threads
  std::vector<std::thread> v;
  v.reserve(threads - 1);
  for (auto i = threads - 1; i > 0; --i)
    v.emplace_back([&ioc] { ioc.run(); });

  shutdown_handler = [&ioc](int sig) {
    LOG(INFO) << "Got signal: " << strsignal(sig) << ". Shutting down server";
    ioc.stop();
    std::exit(0);
  };

  LOG(INFO) << "Listening on " << address << ":" << port;
  ioc.run();

  return EXIT_SUCCESS;
}