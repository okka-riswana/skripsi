import argparse
import asyncio
import logging
import time
from abc import abstractmethod
from typing import Dict, Optional

from aioquic.asyncio import QuicConnectionProtocol, serve
from aioquic.h3.connection import H3_ALPN, H3Connection
from aioquic.h3.events import (
    DatagramReceived,
    H3Event,
    HeadersReceived,
    WebTransportStreamDataReceived,
)
from aioquic.quic.configuration import QuicConfiguration
from aioquic.quic.connection import stream_is_unidirectional
from aioquic.quic.events import (
    ProtocolNegotiated,
    QuicEvent,
    StreamReset,
    ConnectionTerminated
)
from aioquic.quic.logger import QuicFileLogger
from aioquic.tls import SessionTicket

try:
    import uvloop
except ImportError:
    uvloop = None


class WebTransportHandler:

    def __init__(self, stream_id: int, connection: H3Connection) -> None:
        self._connection = connection
        self._connect_stream_id = stream_id
        self._logger = logging.getLogger(self.__class__.__name__)

    @abstractmethod
    def stream_received(self, stream_id: int, data: bytes) -> None:
        pass

    @abstractmethod
    def unistream_received(self, stream_id: int, data: bytes) -> None:
        pass

    @abstractmethod
    def datagram_received(self, flow_id: int, data: bytes) -> None:
        pass

    @abstractmethod
    def session_closed(self) -> None:
        pass

    @abstractmethod
    def stream_fin_received(self, stream_id: int) -> None:
        pass

    def h3_event_received(self, event: H3Event) -> None:
        if isinstance(event, WebTransportStreamDataReceived):
            stream_id = event.stream_id
            if stream_is_unidirectional(stream_id):
                self.unistream_received(stream_id, event.data)
            else:
                self.stream_received(stream_id, event.data)
            if event.stream_ended:
                self.stream_fin_received(stream_id)
        elif isinstance(event, DatagramReceived):
            self.datagram_received(event.flow_id, event.data)

    def _send_stream_data(self,
                          stream_id: int,
                          data: bytes,
                          end_stream: bool = False) -> None:
        self._connection._quic.send_stream_data(stream_id, data, end_stream)

    def _send_datagram(self, flow_id: int, data: bytes) -> None:
        self._connection.send_datagram(flow_id, data)

    def _create_stream(self, is_unidirectional: bool = False) -> int:
        return self._connection.create_webtransport_stream(
            self._connect_stream_id,
            is_unidirectional
        )


class EchoHandler(WebTransportHandler):

    def stream_received(self, stream_id: int, data: bytes) -> None:
        self._send_stream_data(stream_id, data)

    def unistream_received(self, stream_id: int, data: bytes) -> None:
        resp_stream_id = self._create_stream(is_unidirectional=True)
        self._send_stream_data(resp_stream_id, data, end_stream=True)

    def datagram_received(self, flow_id: int, data: bytes) -> None:
        self._send_datagram(flow_id, data)

    def session_closed(self) -> None:
        self._logger.info(
            f"session closed connect_id={self._connect_stream_id}")

    def stream_fin_received(self, stream_id: int) -> None:
        self._logger.info(f"stream fin stream_id={stream_id}")


def parse_latency(raw_data: bytes) -> (int, int, int):
    """
    Parse sent timestamp and calculate the delta between now and then.

    :param raw_data: ascii encoded sent timestamp in microsecond
    :return: delta, sent, and received (current) timestamps in microsecond
    """
    sent = int(raw_data.decode("ascii"))
    recvd = round(time.time_ns() / 1000)
    delta = recvd - sent
    assert delta >= 0

    return delta, sent, recvd


class LatencyHandler(WebTransportHandler):

    def stream_received(self, stream_id: int, data: bytes) -> None:
        delta, sent, recvd = parse_latency(data)
        self._logger.info(
            f"stream received delta={delta}us sent={sent}us recvd={recvd}us"
        )
        response = str(recvd).encode('ascii')
        self._send_stream_data(stream_id, response)

    def unistream_received(self, stream_id: int, data: bytes) -> None:
        delta, sent, recvd = parse_latency(data)
        self._logger.info(
            f"uni stream received delta={delta}us sent={sent}us recvd={recvd}us"
        )
        response = str(recvd).encode('ascii')
        resp_stream_id = self._create_stream(is_unidirectional=True)
        self._send_stream_data(resp_stream_id, response, end_stream=True)

    def datagram_received(self, flow_id: int, data: bytes) -> None:
        delta, sent, recvd = parse_latency(data)
        self._logger.info(
            f"datagram received delta={delta}us sent={sent}us recvd={recvd}us"
        )
        response = str(recvd).encode('ascii')
        self._send_datagram(flow_id, response)

    def session_closed(self) -> None:
        pass

    def stream_fin_received(self, stream_id: int) -> None:
        pass


class WebTransportProtocol(QuicConnectionProtocol):
    """
    WebTransportProtocol handles the beginning of a WebTransport connection: it
    responds to an extended CONNECT method request, and routes the transport
    events to a relevant handler.
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._http: Optional[H3Connection] = None
        self._handler: Optional[WebTransportHandler] = None

    def quic_event_received(self, event: QuicEvent) -> None:
        if isinstance(event, ProtocolNegotiated):
            self._http = H3Connection(self._quic, enable_webtransport=True)
        elif isinstance(event, (StreamReset, ConnectionTerminated)):
            # Streams in QUIC can be closed in two ways: normal (FIN) and
            # abnormal (resets).  FIN is handled by the handler; the code
            # below handles the resets.
            if self._handler is not None:
                self._handler.session_closed()

        if self._http is not None:
            for h3_event in self._http.handle_event(event):
                self._h3_event_received(h3_event)

    def _h3_event_received(self, event: H3Event) -> None:
        if isinstance(event, HeadersReceived):
            headers = {}
            for header, value in event.headers:
                headers[header] = value
            if (headers.get(b":method") == b"CONNECT" and
                    headers.get(b":protocol") == b"webtransport"):
                self._handshake_webtransport(event.stream_id, headers)
            else:
                self._send_response(event.stream_id, 400, end_stream=True)

        if self._handler is not None:
            self._handler.h3_event_received(event)

    def _handshake_webtransport(self,
                                stream_id: int,
                                request_headers: Dict[bytes, bytes]) -> None:
        authority = request_headers.get(b":authority")
        path = request_headers.get(b":path")
        if authority is None or path is None:
            # `:authority` and `:path` must be provided.
            self._send_response(stream_id, 400, end_stream=True)
            return
        if path == b"/echo":
            assert self._handler is None
            self._handler = EchoHandler(stream_id, self._http)
            self._send_response(stream_id, 200)
        elif path == b"/latency":
            assert self._handler is None
            self._handler = LatencyHandler(stream_id, self._http)
            self._send_response(stream_id, 200)
        else:
            self._send_response(stream_id, 404, end_stream=True)

    def _send_response(self,
                       stream_id: int,
                       status_code: int,
                       end_stream=False) -> None:
        headers = [(b":status", str(status_code).encode())]
        if status_code == 200:
            headers.append((b"sec-webtransport-http3-draft", b"draft02"))
        self._http.send_headers(
            stream_id=stream_id, headers=headers, end_stream=end_stream)


class SessionTicketStore:
    """
    Simple in-memory store for session tickets.
    """

    def __init__(self) -> None:
        self.tickets: Dict[bytes, SessionTicket] = {}

    def add(self, ticket: SessionTicket) -> None:
        self.tickets[ticket.ticket] = ticket

    def pop(self, label: bytes) -> Optional[SessionTicket]:
        return self.tickets.pop(label, None)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="QUIC server")
    parser.add_argument(
        "app",
        type=str,
        nargs="?",
        default="asgi:app",
        help="the ASGI application as <module>:<attribute>",
    )
    parser.add_argument(
        "-c",
        "--certificate",
        type=str,
        required=True,
        help="load the TLS certificate from the specified file",
    )
    parser.add_argument(
        "--host",
        type=str,
        default="::",
        help="listen on the specified address (defaults to ::)",
    )
    parser.add_argument(
        "--port",
        type=int,
        default=4433,
        help="listen on the specified port (defaults to 4433)",
    )
    parser.add_argument(
        "-k",
        "--private-key",
        type=str,
        help="load the TLS private key from the specified file",
    )
    parser.add_argument(
        "-l",
        "--secrets-log",
        type=str,
        help="log secrets to a file, for use with Wireshark",
    )
    parser.add_argument(
        "-q",
        "--quic-log",
        type=str,
        help="log QUIC events to QLOG files in the specified directory",
    )
    parser.add_argument(
        "--retry",
        action="store_true",
        help="send a retry for new connections",
    )
    parser.add_argument(
        "-v", "--verbose", action="store_true",
        help="increase logging verbosity"
    )
    opts = parser.parse_args()

    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(name)s %(message)s",
        level=logging.DEBUG if opts.verbose else logging.INFO,
    )

    # create QUIC logger
    if opts.quic_log:
        quic_logger = QuicFileLogger(opts.quic_log)
    else:
        quic_logger = None

    # open SSL log file
    if opts.secrets_log:
        secrets_log_file = open(opts.secrets_log, "a")
    else:
        secrets_log_file = None

    configuration = QuicConfiguration(
        alpn_protocols=H3_ALPN,
        is_client=False,
        max_datagram_frame_size=65536,
        quic_logger=quic_logger,
        secrets_log_file=secrets_log_file,
    )

    # load SSL certificate and key
    configuration.load_cert_chain(opts.certificate, opts.private_key)

    ticket_store = SessionTicketStore()

    if uvloop is not None:
        uvloop.install()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(
        serve(
            opts.host,
            opts.port,
            configuration=configuration,
            create_protocol=WebTransportProtocol,
            session_ticket_fetcher=ticket_store.pop,
            session_ticket_handler=ticket_store.add,
            retry=opts.retry,
        )
    )
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
