# WebTransport Test Client

1. Open index.html in Google Chrome >=97 with options to trust self-signed certificate.\
   For example, if the server is running on localhost port 20001 and the certificate fingerprint is `35wROCei5JziZPpwkQmKCydYmHOGEEnSvoRcBMJ+2cQ=`,
   ```shell
   google-chrome \
       --origin-to-force-quic-on=localhost:20001 \
       --ignore-certificate-errors-spki-list=35wROCei5JziZPpwkQmKCydYmHOGEEnSvoRcBMJ+2cQ=
   ```
2. View the devtools console
