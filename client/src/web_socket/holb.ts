export async function webSocketEchoHoLB(
  url: string,
  smallStrLength = 64,
  bigStrLength = 512 * 1024,
  iteration = 10,
  smallJitter = 20,
  maxConcurrent = 1
): Promise<HoLTimingResults> {
  const sleep = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  const randomString = (length: number): string => {
    const chars =
      '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < length; ++i) {
      result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
  };

  const socket = new WebSocket(url);
  socket.onerror = () => {
    throw new Error('WebSocket error');
  };
  socket.onclose = (event) => {
    if (!event.wasClean) {
      throw new Error(`WebSocket closed abnormally, code: ${event.code}`);
    }
  };
  await new Promise<void>((resolve) => {
    socket.onopen = () => {
      resolve();
    };
  });

  bigStrLength = iteration.toString().length + 21 + bigStrLength;
  const bigStr = randomString(bigStrLength);
  smallStrLength = iteration.toString().length + 21 + smallStrLength;
  const smallStr = randomString(smallStrLength);

  const bigReq: number[] = [];
  const bigRes: number[] = [];
  const smallReq: number[] = [];
  const smallRes: number[] = [];
  const bigResolves: (() => void)[] = [];
  const smallResolves: (() => void)[] = [];

  socket.onmessage = (event) => {
    const { n, t } = JSON.parse(event.data);
    (t === 'b' ? bigRes : smallRes)[n] = performance.now();
    (t === 'b' ? bigResolves : smallResolves)[n]();
    console.log(t, n);
  };

  type SendType = 'big' | 'small';
  async function send(
    start: number,
    end: number,
    type: SendType
  ): Promise<void> {
    const str = type === 'big' ? bigStr : smallStr;
    for (let i = start; i < end; ++i) {
      if (type === 'small') {
        await sleep(smallJitter);
      }
      const done = new Promise<void>((resolve) => {
        (type === 'big' ? bigResolves : smallResolves)[i] = resolve;
      });
      const data = JSON.stringify({
        n: i,
        t: type[0],
        d: str.substring(21 + i.toString().length),
      });
      (type === 'big' ? bigReq : smallReq)[i] = performance.now();
      socket.send(data);
      await done;
    }
  }

  const msgPerIteration = iteration / maxConcurrent;
  const sends: Promise<void>[] = [];
  for (let i = 0; i < maxConcurrent; ++i) {
    sends.push(send(i * msgPerIteration, (i + 1) * msgPerIteration, 'big'));
    sends.push(send(i * msgPerIteration, (i + 1) * msgPerIteration, 'small'));
  }

  const start = performance.now();
  await Promise.all(sends);
  const elapsed = performance.now() - start;

  socket.close();

  const bigResults: TimingResult[] = [];
  for (let i = 0; i < iteration; ++i) {
    bigResults.push({
      startTime: bigReq[i],
      endTime: bigRes[i],
      delta: bigRes[i] - bigReq[i],
    });
  }
  const smallResults: TimingResult[] = [];
  for (let i = 0; i < iteration; ++i) {
    smallResults.push({
      startTime: smallReq[i],
      endTime: smallRes[i],
      delta: smallRes[i] - smallReq[i],
    });
  }
  return { big: bigResults, small: smallResults, elapsed };
}
