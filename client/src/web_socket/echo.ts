export async function webSocketEcho(
  url: string,
  strLength = 10,
  iteration = 10,
  maxConcurrent = 1
): Promise<TimingResults> {
  const sleep = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  const randomString = (length: number): string => {
    const chars =
      '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < length; ++i) {
      result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
  };

  const socket = new WebSocket(url);
  socket.onerror = () => {
    throw new Error('WebSocket error');
  };
  socket.onclose = (event) => {
    if (!event.wasClean) {
      throw new Error(`WebSocket closed abnormally, code: ${event.code}`);
    }
  };
  await new Promise<void>((resolve) => {
    socket.onopen = () => {
      resolve();
    };
  });

  const str = randomString(strLength);

  const req: number[] = [];
  const res: number[] = [];
  const resolves: (() => void)[] = [];

  socket.onmessage = (event) => {
    const { n } = JSON.parse(event.data);
    res[n] = performance.now();
    resolves[n]();
    console.log(n);
  };

  async function send(start: number, end: number): Promise<void> {
    for (let i = start; i < end; ++i) {
      const done = new Promise<void>((resolve) => {
        resolves[i] = resolve;
      });
      const data = JSON.stringify({
        n: i,
        d: str.substring(13 + i.toString().length),
      });
      req[i] = performance.now();
      socket.send(data);
      await done;
    }
  }

  const msgPerIteration = iteration / maxConcurrent;
  const sends: Promise<void>[] = [];
  for (let i = 0; i < maxConcurrent; ++i) {
    sends.push(send(i * msgPerIteration, (i + 1) * msgPerIteration));
  }
  const start = performance.now();
  await Promise.all(sends);
  const elapsed = performance.now() - start;

  socket.close();

  const results: TimingResult[] = [];
  for (let i = 0; i < iteration; ++i) {
    results.push({
      startTime: req[i],
      endTime: res[i],
      delta: res[i] - req[i],
    });
  }
  return { results, elapsed };
}
