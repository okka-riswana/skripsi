/**
 * IMPORTANT: **This function should be run in browser context**
 *
 * @param url WebTransport endpoint
 * @returns Test results
 */
export async function webTransportBidiEcho(
  url: string,
  strLength = 10,
  iteration = 10,
  maxConcurrent = 1
): Promise<TimingResults> {
  // Text encoder and decoder
  const encoder = new TextEncoder();
  const decoder = new TextDecoder('utf-8', { ignoreBOM: true });

  const randomString = (length: number): string => {
    const chars =
      '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < length; ++i) {
      result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
  };

  function fromVarInt(arr: Uint8Array): [number, number] {
    let v = arr[0];
    const length = 1 << (v >> 6);

    if (arr.length < length) {
      return [0, length];
    }

    v &= 0x3f;
    for (let i = 0; i < length - 1; ++i) {
      v = (v << 8) + arr[i + 1];
    }
    if (!Number.isSafeInteger(v)) {
      throw new Error('VarInt overflow');
    }
    return [v, length];
  }

  function toVarInt(length: number): Uint8Array {
    if (length <= 63) {
      return Uint8Array.of(length);
    } else if (length <= 16383) {
      return Uint8Array.of((length >> 8) | 0x40, length);
    } else if (length <= 1073741823) {
      return Uint8Array.of(
        (length >> 24) | 0x80,
        length >> 16,
        length >> 8,
        length
      );
    } else if (length <= Number.MAX_SAFE_INTEGER) {
      return Uint8Array.of(
        (length >> 56) | 0xc0,
        length >> 48,
        length >> 40,
        length >> 32,
        length >> 24,
        length >> 16,
        length >> 8,
        length
      );
    } else {
      throw new Error(`length = ${length} is bigger than max safe integer`);
    }
  }

  function createIncomingStreamHandler(
    callback: IncomingStreamDataCallback,
    endCallback?: IncomingStreamEndCallback
  ): WritableStream<Uint8Array> {
    let messageLength = 0;
    let varIntLength = 0;
    let buffer = new Uint8Array(0);
    const enum State {
      Idle = 0,
      WaitingLength = 1,
      WaitingMessage = 2,
    }
    let state = State.Idle;
    return new WritableStream<Uint8Array>({
      write: async (chunk: Uint8Array) => {
        const oldBuffer = buffer;
        buffer = new Uint8Array(oldBuffer.length + chunk.length);
        buffer.set(oldBuffer);
        buffer.set(chunk, oldBuffer.length);
        do {
          if (state === State.Idle || state === State.WaitingLength) {
            [messageLength, varIntLength] = fromVarInt(buffer);
            state =
              buffer.length < varIntLength
                ? State.WaitingLength
                : State.WaitingMessage;
          }
          if (
            state === State.WaitingLength ||
            (state === State.WaitingMessage &&
              buffer.length < messageLength + varIntLength)
          ) {
            break;
          }
          const data = buffer.slice(varIntLength, messageLength + varIntLength);
          callback(data);
          buffer = buffer.slice(messageLength + varIntLength);
          state = State.Idle;
        } while (buffer.length !== 0);
      },
      close: async () => {
        if (endCallback) {
          endCallback(/*clean=*/ true);
        }
      },
      abort: async (reason) => {
        if (endCallback) {
          endCallback(/*clean=*/ false, reason);
        }
      },
    });
  }

  const transport = new WebTransport(url);
  await transport.ready;
  transport.closed.then(() => {
    console.info('session closed');
  });

  const str = randomString(strLength);

  const req: number[] = [];
  const res: number[] = [];

  async function send(start: number, end: number): Promise<void> {
    const bidiStream = await transport.createBidirectionalStream();
    const bidiWriter = bidiStream.writable.getWriter();
    const resolves: (() => void)[] = [];

    bidiStream.readable.pipeTo(
      createIncomingStreamHandler((rawData) => {
        const { n } = JSON.parse(decoder.decode(rawData));
        res[n] = performance.now();
        resolves[n]();
        console.log(n);
      })
    );

    for (let i = start; i < end; ++i) {
      const done = new Promise<void>((resolve) => {
        resolves[i] = resolve;
      });
      const payload = JSON.stringify({
        n: i,
        d: str.substring(13 + i.toString().length),
      });
      req[i] = performance.now();
      const prefix = toVarInt(payload.length);
      const encoded = encoder.encode(payload);
      const data = new Uint8Array(prefix.length + encoded.length);
      data.set(prefix);
      data.set(encoded, prefix.length);
      await bidiWriter.write(data);
      await done;
    }
    await bidiWriter.close();
  }

  const msgPerIteration = iteration / maxConcurrent;
  const sends: Promise<void>[] = [];
  for (let i = 0; i < maxConcurrent; ++i) {
    sends.push(send(i * msgPerIteration, (i + 1) * msgPerIteration));
  }

  const start = performance.now();
  await Promise.all(sends);
  const elapsed = performance.now() - start;

  transport.close();

  const results: TimingResult[] = [];
  for (let i = 0; i < iteration; ++i) {
    results.push({
      startTime: req[i],
      endTime: res[i],
      delta: res[i] - req[i],
    });
  }

  return { results, elapsed };
}
