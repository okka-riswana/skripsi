export async function webTransportBidiHoLB(
  url: string,
  smallStrLength = 64,
  bigStrLength = 512 * 1024,
  iteration = 10,
  smallJitter = 20,
  maxConcurrent = 1
): Promise<HoLTimingResults> {
  const sleep = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  // Text encoder and decoder
  const encoder = new TextEncoder();
  const decoder = new TextDecoder('utf-8', { ignoreBOM: true });

  const randomString = (length: number): string => {
    const chars =
      '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < length; ++i) {
      result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
  };

  function fromVarInt(arr: Uint8Array): [number, number] {
    let v = arr[0];
    const length = 1 << (v >> 6);

    if (arr.length < length) {
      return [0, length];
    }

    v &= 0x3f;
    for (let i = 0; i < length - 1; ++i) {
      v = (v << 8) + arr[i + 1];
    }
    if (!Number.isSafeInteger(v)) {
      throw new Error('VarInt overflow');
    }
    return [v, length];
  }

  function toVarInt(length: number): Uint8Array {
    if (length <= 63) {
      return Uint8Array.of(length);
    } else if (length <= 16383) {
      return Uint8Array.of((length >> 8) | 0x40, length);
    } else if (length <= 1073741823) {
      return Uint8Array.of(
        (length >> 24) | 0x80,
        length >> 16,
        length >> 8,
        length
      );
    } else if (length <= Number.MAX_SAFE_INTEGER) {
      return Uint8Array.of(
        (length >> 56) | 0xc0,
        length >> 48,
        length >> 40,
        length >> 32,
        length >> 24,
        length >> 16,
        length >> 8,
        length
      );
    } else {
      throw new Error(`length = ${length} is bigger than max safe integer`);
    }
  }

  type IncomingStreamDataCallback = (data: Uint8Array) => void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  type IncomingStreamEndCallback = (clean?: boolean, reason?: any) => void;

  function createIncomingStreamHandler(
    callback: IncomingStreamDataCallback,
    endCallback?: IncomingStreamEndCallback
  ): WritableStream<Uint8Array> {
    let messageLength = 0;
    let varIntLength = 0;
    let buffer = new Uint8Array(0);
    const enum State {
      Idle = 0,
      WaitingLength = 1,
      WaitingMessage = 2,
    }
    let state = State.Idle;
    return new WritableStream<Uint8Array>({
      write: async (chunk: Uint8Array) => {
        const oldBuffer = buffer;
        buffer = new Uint8Array(oldBuffer.length + chunk.length);
        buffer.set(oldBuffer);
        buffer.set(chunk, oldBuffer.length);
        while (buffer.length !== 0) {
          if (state === State.Idle || state === State.WaitingLength) {
            [messageLength, varIntLength] = fromVarInt(buffer);
            state =
              buffer.length < varIntLength
                ? State.WaitingLength
                : State.WaitingMessage;
          }
          if (
            state === State.WaitingLength ||
            (state === State.WaitingMessage &&
              buffer.length < messageLength + varIntLength)
          ) {
            break;
          }
          const data = buffer.slice(varIntLength, messageLength + varIntLength);
          callback(data);
          buffer = buffer.slice(messageLength + varIntLength);
          state = State.Idle;
        }
      },
      close: async () => {
        if (endCallback) {
          endCallback(/*clean=*/ true);
        }
      },
      abort: async (reason) => {
        if (endCallback) {
          endCallback(/*clean=*/ false, reason);
        }
      },
    });
  }

  bigStrLength = iteration.toString().length + 21 + bigStrLength;
  const bigStr = randomString(bigStrLength);
  smallStrLength = iteration.toString().length + 21 + smallStrLength;
  const smallStr = randomString(smallStrLength);

  const transport = new WebTransport(url);
  await transport.ready;
  transport.closed.then(() => {
    console.info('session closed');
  });

  const bigReq: number[] = [];
  const bigRes: number[] = [];
  const smallReq: number[] = [];
  const smallRes: number[] = [];
  const bigResolves: (() => void)[] = [];
  const smallResolves: (() => void)[] = [];

  type SendType = 'big' | 'small';

  async function send(
    start: number,
    end: number,
    type: SendType
  ): Promise<void> {
    const str = type === 'big' ? bigStr : smallStr;
    const bidiStream = await transport.createBidirectionalStream();
    const bidiWriter = bidiStream.writable.getWriter();

    bidiStream.readable.pipeTo(
      createIncomingStreamHandler((data) => {
        const { n, t } = JSON.parse(decoder.decode(data));
        (t === 'b' ? bigRes : smallRes)[n] = performance.now();
        (t === 'b' ? bigResolves : smallResolves)[n]();
        console.log(t, n);
      })
    );
    for (let i = start; i < end; ++i) {
      if (type === 'small') {
        await sleep(smallJitter);
      }
      const done = new Promise<void>((resolve) => {
        (type === 'big' ? bigResolves : smallResolves)[i] = resolve;
      });
      const payload = JSON.stringify({
        n: i,
        t: type[0],
        d: str.substring(21 + i.toString().length),
      });
      (type === 'big' ? bigReq : smallReq)[i] = performance.now();
      const prefix = toVarInt(payload.length);
      const encoded = encoder.encode(payload);
      const data = new Uint8Array(prefix.length + encoded.length);
      data.set(prefix);
      data.set(encoded, prefix.length);
      await bidiWriter.write(data);
      await done;
    }
    await bidiWriter.close();
  }

  const msgPerIteration = iteration / maxConcurrent;
  const sends: Promise<void>[] = [];
  for (let i = 0; i < maxConcurrent; ++i) {
    sends.push(send(i * msgPerIteration, (i + 1) * msgPerIteration, 'big'));
    sends.push(send(i * msgPerIteration, (i + 1) * msgPerIteration, 'small'));
  }
  const start = performance.now();
  await Promise.all(sends);
  const elapsed = performance.now() - start;

  transport.close();

  const bigResults: TimingResult[] = [];
  for (let i = 0; i < iteration; ++i) {
    bigResults.push({
      startTime: bigReq[i],
      endTime: bigRes[i],
      delta: bigRes[i] - bigReq[i],
    });
  }
  const smallResults: TimingResult[] = [];
  for (let i = 0; i < iteration; ++i) {
    smallResults.push({
      startTime: smallReq[i],
      endTime: smallRes[i],
      delta: smallRes[i] - smallReq[i],
    });
  }
  return { big: bigResults, small: smallResults, elapsed };
}
