/**
 * IMPORTANT: **This function should be run in browser context**
 *
 * @param url WebTransport endpoint
 * @returns Test results
 */
export async function webTransportDatagramEcho(
  url: string,
  strLength = 10,
  iteration = 10
): Promise<TimingResults> {
  const sleep = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  // Text encoder and decoder
  const encoder = new TextEncoder();
  const decoder = new TextDecoder();

  const randomString = (length: number): string => {
    const chars =
      '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    for (let i = 0; i < length; ++i) {
      result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
  };

  type IncomingStreamDataCallback = (data: Uint8Array) => void;
  type IncomingStreamEndCallback = () => void;

  function createStreamHandler(
    callback: IncomingStreamDataCallback,
    endCallback?: IncomingStreamEndCallback
  ) {
    return new WritableStream<Uint8Array>({
      write: (chunk: Uint8Array) => {
        return new Promise((resolve) => {
          callback(chunk);
          resolve();
        });
      },
      close: () => {
        if (endCallback) {
          endCallback();
        }
      },
    });
  }

  const transport = new WebTransport(url);
  await transport.ready;
  transport.closed.then(() => {
    console.info('session closed');
  });

  const datagramWriter = transport.datagrams.writable.getWriter();

  const str = randomString(strLength);
  const timeout = 10;

  const req: number[] = [];
  const res: number[] = [];
  const resolves: (() => void)[] = [];

  transport.datagrams.readable.pipeTo(
    createStreamHandler((rawData) => {
      const { n } = JSON.parse(decoder.decode(rawData));
      res[n] = performance.now();
      resolves[n]();
      console.log(n);
    })
  );

  const start = performance.now();
  for (let i = 0; i < iteration; ++i) {
    const done = new Promise<void>((resolve) => {
      resolves[i] = resolve;
      setTimeout(resolve, timeout);
    });
    const payload = JSON.stringify({
      n: i,
      d: str.substring(13 + i.toString().length),
    });
    req[i] = performance.now();
    await datagramWriter.write(encoder.encode(payload));
    await done;
  }
  const elapsed = performance.now() - start;

  await datagramWriter.close();
  transport.close();

  const results: TimingResult[] = [];
  for (let i = 0; i < iteration; ++i) {
    results.push({
      startTime: req[i],
      endTime: res[i] || -1,
      delta: Number.isNaN(req[i]) ? -1 : res[i] - req[i],
    });
  }

  return { results, elapsed };
}
