import { stringify } from 'csv';
import { execSync } from 'node:child_process';
import cluster from 'node:cluster';
import { createWriteStream, readFileSync, writeFileSync } from 'node:fs';
import { join, dirname } from 'node:path';
import { fileURLToPath } from 'node:url';
import puppeteer, { EvaluateFn } from 'puppeteer';
import { webSocketEcho, webSocketEchoHoLB } from './web_socket';
import {
  webTransportBidiEcho,
  webTransportBidiHoLB,
  webTransportDatagramEcho,
  webTransportUniEcho,
} from './web_transport';
import { webTransportBidiDatagramHoLB } from './web_transport/bidi_stream_datagram_holb';

const __dirname = dirname(fileURLToPath(import.meta.url));

const server = process.argv[2] || 'server.skripsi.lan';
const certFingerprint =
  process.argv[3] || 'lK0W7yRJjoRM5HYLaSlqDU5HoAb7LgbUt/2+CJaPaxk=';
const webSocketPort = parseInt(process.argv[4], 10) || 8080;
const webTransportPort = parseInt(process.argv[5], 10) || 4433;
const numBrowsers = parseInt(process.argv[6], 10) || 1;

const trafficControlScript = join(__dirname, 'traffic-control.sh');

interface Test {
  name: string;
  url: string;
  proto: 'ws' | 'wt';
  type: 'reg' | 'hol';
  func: EvaluateFn<any>;
  args: any[];
  tc?: {
    bandwidth?: number;
    latency?: number;
    packetLoss?: number;
  };
}

interface ResultSummary {
  name: string;
  elapsed: number;
  sent: number;
  recvd: number;
  n: number;
  mean: number;
  stddev: number;
  stderr: number;
  ci: number;
  median: number;
  mc: number;
  firstquartile: number;
  thirdquartile: number;
  lowerwhisker: number;
  upperwhisker: number;
  max: number;
  min: number;
}

function medcouple(arr: number[]): number {
  // sort decreasing
  const X = arr.sort((a, b) => b - a);
  const xscale = 2 * Math.max(...X);
  // const xscale = 1;
  const xm = X[Math.floor(X.length / 2)];
  const Zplus = X.filter((x) => x >= xm).map((x) => (x - xm) / xscale);
  const Zminus = X.filter((x) => x <= xm).map((x) => (x - xm) / xscale);

  const p = Zplus.length;
  const q = Zminus.length;

  function h(i: number, j: number): number {
    const a = Zplus[i];
    const b = Zminus[j];
    if (a === b) {
      return Math.sign(p - 1 - i - j);
    }
    return (a + b) / (a - b);
  }

  const H = new Array<number>();
  for (let i = 0; i < p; i++) {
    for (let j = 0; j < q; j++) {
      H.push(h(i, j));
    }
  }

  const sorted_H = H.map(
    (x) => Math.round((x + Number.EPSILON) * 100) / 100
  ).sort((a, b) => a - b);

  return sorted_H[Math.floor(sorted_H.length / 2)];
}

function processResults(
  name: string,
  results: TimingResult[],
  elapsed: number
): ResultSummary {
  const sent = results.reduce(
    (acc, { startTime }) => (!!startTime && startTime >= 0 ? acc + 1 : acc),
    0
  );

  const recvd = results.reduce(
    (acc, { endTime }) => (!!endTime && endTime >= 0 ? acc + 1 : acc),
    0
  );

  // exclude invalid results
  const deltas = results
    .filter(({ delta }) => !!delta && delta >= 0)
    .map(({ delta }) => delta);

  const sorted_deltas = deltas.map((delta) => delta).sort((a, b) => a - b);

  const n = deltas.length;

  const mean = results.reduce((acc, { delta }) => acc + delta, 0) / n;

  const stddev = Math.sqrt(
    deltas
      .map((delta) => Math.pow(delta - mean, 2))
      .reduce((acc, cur) => acc + cur) / n
  );

  const stderr = stddev / Math.sqrt(n);

  // 95% confidence interval
  const ci = 1.96 * stderr;

  const max = sorted_deltas[n - 1];
  const min = sorted_deltas[0];

  const median = sorted_deltas[Math.floor(n / 2)];
  const firstquartile = sorted_deltas[Math.floor(n / 4)];
  const thirdquartile = sorted_deltas[Math.floor((3 * n) / 4)];

  const iqr = thirdquartile - firstquartile;
  const mc = medcouple(deltas);

  let lowerwhisker: number;
  let upperwhisker: number;
  if (mc >= 0) {
    lowerwhisker = firstquartile - 1.5 * Math.pow(Math.E, -4 * mc) * iqr;
    upperwhisker = thirdquartile + 1.5 * Math.pow(Math.E, 3 * mc) * iqr;
  } else {
    lowerwhisker = firstquartile - 1.5 * Math.pow(Math.E, -3 * mc) * iqr;
    upperwhisker = thirdquartile + 1.5 * Math.pow(Math.E, 4 * mc) * iqr;
  }

  stringify(
    deltas.map((d) => ({ [`${name.split('-').at(-1)}`]: d })),
    { header: true }
  ).pipe(createWriteStream(`${process.cwd()}/${name}.csv`));

  return {
    name,
    elapsed,
    sent,
    recvd,
    n,
    mean,
    stddev,
    stderr,
    ci,
    median,
    mc,
    firstquartile,
    thirdquartile,
    lowerwhisker,
    upperwhisker,
    max,
    min,
  };
}

async function runTest(
  suiteName: string,
  test: Test,
  useCache = true
): Promise<ResultSummary[]> {
  console.info('Running Test', test);
  const cacheFileName = `${process.cwd()}/${suiteName}-${test.name}.json`;

  if (useCache) {
    try {
      const rawResults = JSON.parse(readFileSync(cacheFileName, 'utf8'));
      console.log('Using cached results from', cacheFileName);
      if (test.type === 'reg') {
        const result = rawResults as TimingResults;
        return [
          processResults(
            `${suiteName}-${test.name}`,
            result.results,
            result.elapsed
          ),
        ];
      } else if (test.type === 'hol') {
        const result = rawResults as HoLTimingResults;
        return [
          processResults(
            `${suiteName}-${test.name}_big`,
            result.big,
            result.elapsed
          ),
          processResults(
            `${suiteName}-${test.name}_small`,
            result.small,
            result.elapsed
          ),
        ];
      }
    } catch (e) {
      console.log('unable to read cache, starting fresh');
    }
  }

  const browserArgs = [
    `--ignore-certificate-errors-spki-list=${certFingerprint}`,
  ];
  if (test.proto === 'wt') {
    browserArgs.push(`--origin-to-force-quic-on=${server}:${webTransportPort}`);
  }

  const browser = await puppeteer.launch({
    product: 'chrome',
    args: browserArgs,
    dumpio: false,
    ignoreHTTPSErrors: true,
  });

  const page = await browser.newPage();
  await page.goto(new URL('index.html', import.meta.url).href);
  console.info('User Agent:', await page.evaluate('navigator.userAgent'));
  page.on('console', (message) =>
    console.log(
      `(${suiteName})(${test.name})[${message
        .type()
        .substring(0, 3)
        .toUpperCase()}] ${message.text()}`
    )
  );
  page.on('error', (msg) => {
    throw msg;
  });

  const rawResults: TimingResults | HoLTimingResults = await page.evaluate(
    test.func,
    test.url,
    ...test.args
  );

  await browser.close();

  if (useCache) {
    writeFileSync(cacheFileName, JSON.stringify(rawResults));
  }

  if (test.type === 'reg') {
    const result = rawResults as TimingResults;
    return [
      processResults(
        `${suiteName}-${test.name}`,
        result.results,
        result.elapsed
      ),
    ];
  } else if (test.type === 'hol') {
    const result = rawResults as HoLTimingResults;
    return [
      processResults(
        `${suiteName}-${test.name}_big`,
        result.big,
        result.elapsed
      ),
      processResults(
        `${suiteName}-${test.name}_small`,
        result.small,
        result.elapsed
      ),
    ];
  }
}

function setupTc(
  packetLoss = 1,
  bandwidth = 50,
  delay = 20,
  corrupt = 0
): void {
  const serverIP = execSync(`dig +short ${server}`).toString().trim();
  console.info(
    `Setting up traffic control for ${serverIP}: ${packetLoss}% loss, ${bandwidth}Mbps, ${delay}ms delay, ${corrupt}% corrupt`
  );
  const tcCommand = `sudo ${trafficControlScript} --dspeed=${bandwidth}mbit --uspeed=${bandwidth}mbit --delay=${
    delay > 0 ? delay / 2 : 0
  } --loss=${packetLoss > 0 ? packetLoss / 2 : 0} --corrupt=${
    corrupt > 0 ? corrupt / 2 : 0
  } ${serverIP}`;
  console.info('command:', tcCommand);
  execSync(tcCommand);
}

function resetTc(): void {
  console.info('Resetting traffic control');
  execSync(`sudo ${trafficControlScript} -r`);
}

async function main(): Promise<void> {
  /// BASE TESTS

  const smallTests = [32, 64, 128, 256, 512].map((size) => ({
    name: `${size}B`,
    size: size,
    iteration: 1000,
  }));

  const mediumTests = [1, 2, 4, 8, 16].map((size) => ({
    name: `${size}KiB`,
    size: size * 1024,
    iteration: 1000,
  }));

  const bigTests = [32, 64, 128, 256, 512].map((size) => ({
    name: `${size}KiB`,
    size: size * 1024,
    iteration: 1000,
  }));

  const holTests = [
    [64, 64 * 1024],
    [512, 4 * 1024],
  ].map((sizes) => {
    const smallName =
      sizes[0] >= 1024 ? `${sizes[0] / 1024}KiB` : `${sizes[0]}B`;
    const bigName = sizes[1] >= 1024 ? `${sizes[1] / 1024}KiB` : `${sizes[1]}B`;
    return {
      name: `${smallName}_${bigName}`,
      smallSize: sizes[0],
      bigSize: sizes[1],
      iteration: 1000,
      smallJitter: 20,
    };
  });

  ///

  const webSocketTests: Test[] = [
    ...smallTests,
    ...mediumTests,
    ...bigTests,
  ].map((t) => {
    return {
      name: t.name,
      proto: 'ws',
      url: `wss://${server}:${webSocketPort}`,
      type: 'reg',
      func: webSocketEcho,
      args: [t.size, t.iteration],
    };
  });

  const webTransportBidiTests: Test[] = [
    ...smallTests,
    ...mediumTests,
    ...bigTests,
  ].map((t) => {
    return {
      name: t.name,
      proto: 'wt',
      url: `https://${server}:${webTransportPort}/echo`,
      type: 'reg',
      func: webTransportBidiEcho,
      args: [t.size, t.iteration],
    };
  });

  const webTransportUniTests: Test[] = [
    ...smallTests,
    ...mediumTests,
    ...bigTests,
  ].map((t) => {
    return {
      name: t.name,
      proto: 'wt',
      url: `https://${server}:${webTransportPort}/echo`,
      type: 'reg',
      func: webTransportUniEcho,
      args: [t.size, t.iteration],
    };
  });

  const webTransportDatagramTests: Test[] = smallTests.map((t) => {
    return {
      name: t.name,
      proto: 'wt',
      url: `https://${server}:${webTransportPort}/echo`,
      type: 'reg',
      func: webTransportDatagramEcho,
      args: [t.size, t.iteration],
    };
  });

  const wsHoLB: Test[] = holTests.map((t) => ({
    name: `holb_${t.name}`,
    proto: 'ws',
    url: `wss://${server}:${webSocketPort}`,
    type: 'hol',
    func: webSocketEchoHoLB,
    args: [t.smallSize, t.bigSize, t.iteration, t.smallJitter],
  }));

  const wtHoLB: Test[] = holTests.map((t) => ({
    name: `holb_${t.name}`,
    proto: 'wt',
    url: `https://${server}:${webTransportPort}/echo`,
    type: 'hol',
    func: webTransportBidiHoLB,
    args: [t.smallSize, t.bigSize, t.iteration, t.smallJitter],
  }));

  // HoLB Tests
  webSocketTests.push(...wsHoLB);
  webTransportBidiTests.push(...wtHoLB);

  const wsConcurrents: Test[] = [16, 32, 64].map((size) => {
    return {
      name: `10_concurrent_${size}KiB`,
      proto: 'ws',
      url: `wss://${server}:${webSocketPort}`,
      type: 'reg',
      func: webSocketEcho,
      args: [size * 1024, 1000, 10],
    };
  });

  const wtConcurrents: Test[] = [16, 32, 64].map((size) => {
    return {
      name: `10_concurrent_${size}KiB`,
      proto: 'wt',
      url: `https://${server}:${webTransportPort}/echo`,
      type: 'reg',
      func: webTransportBidiEcho,
      args: [size * 1024, 1000, 10],
    };
  });

  webSocketTests.push(...wsConcurrents);
  webTransportBidiTests.push(...wtConcurrents);

  const suites = [
    {
      suiteName: 'ws',
      tests: webSocketTests,
    },
    {
      suiteName: 'wt_bidistream',
      tests: webTransportBidiTests,
    },
    {
      suiteName: 'wt_unistream',
      tests: webTransportUniTests,
    },
    {
      suiteName: 'wt_dgram',
      tests: webTransportDatagramTests,
    },
    {
      suiteName: 'loss_ws',
      tests: new Array(16)
        .fill({
          proto: 'ws',
          url: `wss://${server}:${webSocketPort}/`,
          type: 'reg',
          func: webSocketEcho,
          args: [1 * 1024, 10000],
        })
        .map(
          (t, i): Test => ({
            ...t,
            name: `${i}`,
            tc: { bandwidth: 50, latency: 20, packetLoss: i },
          })
        ),
    },
    {
      suiteName: 'loss_wt_bidi',
      tests: new Array(16)
        .fill({
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'reg',
          func: webTransportBidiEcho,
          args: [1 * 1024, 10000],
        })
        .map(
          (t, i): Test => ({
            ...t,
            name: `${i}`,
            tc: { bandwidth: 50, latency: 20, packetLoss: i },
          })
        ),
    },
    {
      suiteName: 'loss_wt_dgram',
      tests: new Array(16)
        .fill({
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'reg',
          func: webTransportDatagramEcho,
          args: [1 * 1024, 10000],
        })
        .map(
          (t, i): Test => ({
            ...t,
            name: `${i}`,
            tc: { bandwidth: 50, latency: 20, packetLoss: i },
          })
        ),
    },
    {
      suiteName: 'goodput_ws',
      tests: new Array(16)
        .fill({
          proto: 'ws',
          url: `wss://${server}:${webSocketPort}/`,
          type: 'reg',
          func: webSocketEcho,
          args: [1 * 1024 * 1024, 100],
        })
        .map(
          (t, i): Test => ({
            ...t,
            name: `${i}`,
            tc: { bandwidth: 50, latency: 20, packetLoss: i },
          })
        ),
    },
    {
      suiteName: 'goodput_wt_bidi',
      tests: new Array(16)
        .fill({
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'reg',
          func: webTransportBidiEcho,
          args: [1 * 1024 * 1024, 100],
        })
        .map(
          (t, i): Test => ({
            ...t,
            name: `${i}`,
            tc: { bandwidth: 50, latency: 20, packetLoss: i },
          })
        ),
    },
    {
      suiteName: 'delay_ws',
      tests: new Array(7)
        .fill({
          proto: 'ws',
          url: `wss://${server}:${webSocketPort}/`,
          type: 'reg',
          func: webSocketEcho,
          args: [1 * 1024, 1000],
        })
        .map(
          (t, i): Test => ({
            ...t,
            name: `${i * 50}`,
            tc: { bandwidth: 50, latency: i * 50, packetLoss: 0 },
          })
        ),
    },
    {
      suiteName: 'delay_wt_bidi',
      tests: new Array(7)
        .fill({
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'reg',
          func: webTransportBidiEcho,
          args: [1 * 1024, 1000],
        })
        .map(
          (t, i): Test => ({
            ...t,
            name: `${i * 50}`,
            tc: { bandwidth: 50, latency: i * 50, packetLoss: 0 },
          })
        ),
    },
    {
      suiteName: 'delay_wt_dgram',
      tests: new Array(7)
        .fill({
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'reg',
          func: webTransportDatagramEcho,
          args: [1 * 1024, 1000],
        })
        .map(
          (t, i): Test => ({
            ...t,
            name: `${i * 50}`,
            tc: { bandwidth: 50, latency: i * 50, packetLoss: 0 },
          })
        ),
    },
    {
      suiteName: 'small_ws',
      tests: [
        {
          name: '128B-300ms',
          proto: 'ws',
          url: `wss://${server}:${webSocketPort}/`,
          type: 'reg',
          func: webSocketEcho,
          args: [128, 1000],
          tc: { bandwidth: 50, latency: 300, packetLoss: 1 },
        },
      ] as Test[],
    },
    {
      suiteName: 'small_wt_bidi',
      tests: [
        {
          name: '128B-300ms',
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'reg',
          func: webTransportBidiEcho,
          args: [128, 1000],
          tc: { bandwidth: 50, latency: 300, packetLoss: 1 },
        },
      ] as Test[],
    },
    {
      suiteName: 'small_wt_dgram',
      tests: [
        {
          name: '128B-300ms',
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'reg',
          func: webTransportDatagramEcho,
          args: [128, 1000],
          tc: { bandwidth: 50, latency: 300, packetLoss: 1 },
        },
      ] as Test[],
    },
    {
      suiteName: 'holb',
      tests: [
        {
          name: 'wt_bidi_dgram',
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'hol',
          func: webTransportBidiDatagramHoLB,
          args: [128, 128, 10000, 0],
        },
        {
          name: 'wt_bidi',
          proto: 'wt',
          url: `https://${server}:${webTransportPort}/echo`,
          type: 'hol',
          func: webTransportBidiHoLB,
          args: [128, 128, 10000, 0],
        },
        {
          name: 'ws',
          proto: 'ws',
          url: `wss://${server}:${webSocketPort}/`,
          type: 'hol',
          func: webSocketEchoHoLB,
          args: [128, 128, 10000, 0],
        },
      ] as Test[],
    },
  ];

  for (const suite of suites) {
    const summaries = [];
    for (const test of suite.tests) {
      if (test.tc) {
        setupTc(test.tc.packetLoss, test.tc.bandwidth, test.tc.latency);
      }
      const result = await runTest(suite.suiteName, test);
      summaries.push(...result);
      console.table(summaries);
      if (test.tc) {
        resetTc();
      }
    }
    const summaryWriteStream = createWriteStream(
      `${process.cwd()}/${suite.suiteName}.csv`
    );
    stringify(summaries, { header: true }).pipe(summaryWriteStream);
  }
}

if (cluster.isMaster) {
  console.log(`Primary ${process.pid} is running`);

  console.info(`Starting ${numBrowsers} client(s)`);
  for (let i = 0; i < numBrowsers; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  console.log(`Worker ${process.pid} started`);
  await main();
}
