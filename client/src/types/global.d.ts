type ReceiveStream = ReadableStream<Uint8Array>;
type SendStream = WritableStream;

interface WebTransportBidirectionalStream {
  readonly readable: ReceiveStream;
  readonly writable: SendStream;
}

interface WebTransportCloseInfo {
  closeCode: number;
  reason: string;
}

interface WebTransportDatagramDuplexStream {
  readonly readable: ReceiveStream;
  readonly writable: SendStream;

  readonly maxDatagramSize: number;
  incomingMaxAge: number;
  outgoingMaxAge: number;
  incomingHighWatermark: number;
  outgoingHighWatermark: number;
}

declare class WebTransport {
  constructor(url: string, options?: unknown);

  createBidirectionalStream(): Promise<WebTransportBidirectionalStream>;
  createUnidirectionalStream(): Promise<SendStream>;
  close(): undefined;

  readonly ready: Promise<undefined>;
  readonly closed: Promise<WebTransportCloseInfo>;
  readonly incomingBidirectionalStreams: ReadableStream<WebTransportBidirectionalStream>;
  readonly incomingUnidirectionalStreams: ReadableStream<ReceiveStream>;
  readonly datagrams: WebTransportDatagramDuplexStream;
}

type IncomingStreamDataCallback = (data: Uint8Array) => void;
type IncomingStreamEndCallback = (clean?: boolean, reason?: any) => void;

interface TimingResult {
  startTime: number;
  endTime: number;
  delta: number;
}

interface TimingResults {
  results: TimingResult[];
  elapsed: number;
}

interface HoLTimingResults {
  big: TimingResult[];
  small: TimingResult[];
  elapsed: number;
}
