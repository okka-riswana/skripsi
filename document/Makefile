OUTDIR := build
LATEXMK_ARGS := \
	-pdf -pdflatex=lualatex \
	-shell-escape -synctex=1 \
	-interaction=nonstopmode -file-line-error -halt-on-error \
	-use-make -outdir=$(OUTDIR)

GDRIVE_DIR_ID ?= 17ndBJnXxelk7T41yweeJmmhQme4SmO4t
GDRIVE_FILE_PREFIX = $(shell TZ='Asia/Jakarta' date +'%Y-%m-%d-%H-%M-%S')-$(shell git rev-parse --short HEAD)

all: main.pdf presentasi.pdf jurnal.pdf

main.pdf: build-dirs
	latexmk $(LATEXMK_ARGS) main.tex

presentasi.pdf: build-dirs
	latexmk $(LATEXMK_ARGS) presentasi.tex

jurnal.pdf: build-dirs
	latexmk $(LATEXMK_ARGS) jurnal.tex

build-dirs:
	find . -mindepth 1 -name "*.tex" \
		| xargs -L1 dirname \
		| uniq \
		| xargs -I{} mkdir -p ./build/{};

clean:
	latexmk -C
	rm -rfv -- $(OUTDIR)/*.pdf

upload:
	for file in main.pdf presentasi.pdf jurnal.pdf ; do \
		cp "$(OUTDIR)/$${file}" "$(OUTDIR)/$(GDRIVE_FILE_PREFIX)-$$(basename "$${file}")"; \
		gdrive upload --parent $(GDRIVE_DIR_ID) "$(OUTDIR)/$(GDRIVE_FILE_PREFIX)-$$(basename "$${file}")"; \
	done

.PHONY: main.pdf presentasi.pdf all build-dirs clean cleanall upload
