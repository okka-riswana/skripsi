\section{Hasil dan Pembahasan}
\label{sec:hasil_pembahasan}

\subsection{Analisis Hasil Pengukuran RTT}
\label{subsec:hasil_latensi}

Dari hasil pengukuran nilai RTT pada jaringan dengan \foreign{delay} 20ms untuk \foreign{payload}
berukuran 128 byte yang dimana ukuran tersebut berada dibawah nilai MTU (1500 byte), didapatkan
perbedaan yang cukup menonjol diantara WebTransport dan WebSocket seperti yang terlihat pada
\cref{fig:small_20ms}. Mayoritas dari nilai RTT pada semua protokol masih berada pada jangkauan 20,2
sampai 22,5 ms. Namun pada kasus terjadinya \foreign{packet loss}, data yang dikirim menggunakan
WebSocket menjadi \foreign{outlier} di nilai 240--250 ms, dan yang menggunakan \foreign{stream}
WebTransport \foreign{outlier} tersebut berkisar pada 65--68 ms. Untuk WebTransport Datagram, tidak
ada \foreign{outlier} yang terlalu jauh dari 20 ms karena tidak ada retransmisi pada datagram yang
tidak terkirim.

\begin{figure}[ht]
	\centering{}
	\resizebox{\columnwidth}{!}{\input{images/graphs/small_1loss_20ms.tex}}
	\caption{RTT untuk \foreign{Payload} 128 Byte, \foreign{Packet Loss} 1\%, dan \foreign{delay}
		20 ms}
	\label{fig:small_20ms}
\end{figure}

Salah satu penyebab yang dapat memungkinkan perbedaan tersebut berada pada mekanisme retransmisi
pada protokol TCP dan QUIC. Seperti yang dipaparkan pada \cref{subsec:protokol_quic}, protokol QUIC
berada pada \foreign{user space}, sehingga mekanisme ini dapat dikonfigurasi lebih mudah ketimbang
TCP yang mayoritas berada pada tingkat sistem operasi. Koneksi TCP pada Linux memiliki jeda waktu
minimum sebelum retransmisi atau \foreign{Retransmission Timeout} (RTO) ketika data tidak dapat
terkirim dengan benar. Nilai RTO minimum default untuk TCP pada Linux berupa nilai tetap sebesar 200
ms pada \foreign{source code}-nya yang terdapat pada file \texttt{include/net/tcp.h}
\parencite{Pracucci2018}. Untuk mengubah nilai default ini, perlu dilakukan kompilasi ulang pada
kernel, atau dapat diatur per \foreign{route} melalui perintah \foreign{ip}.

Faktor lain seperti algoritma deteksi \foreign{packet loss} dan cara kerja perhitungan nilai RTO
juga dapat berpengaruh kepada nilai akhir dari latensi ini. \Cref{fig:small_300ms} merupakan hasil
perhitungan pada \foreign{payload} dan kondisi \foreign{packet loss} yang sama namun dengan delay
300 ms. Terlihat perbedaan diantara \foreign{stream} WebTransport dan WebSocket relatif lebih kecil
ketimbang pada kondisi delay 20 ms, yang berarti faktor selain nilai RTO minimum turut berpengaruh
kepada nilai latensi.

\begin{figure}[ht]
	\centering{}
	\resizebox{\columnwidth}{!}{\input{images/graphs/small_1loss_300ms.tex}}
	\caption{RTT untuk \foreign{Payload} 128 Byte, \foreign{Packet Loss} 1\%, dan \foreign{delay}
		300 ms}
	\label{fig:small_300ms}
\end{figure}

Secara garis besar, dari hasil-hasil pengukuran diatas terlihat bahwa semua protokol memiliki
besar latensi yang mirip dan tidak terlalu jauh dari latensi pada tingkat jaringan, terkecuali pada
kasus \foreign{packet loss}. Ketika terjadi \foreign{packet loss}, WebTransport Stream memiliki
nilai latensi yang jauh lebih kecil ketimbang WebSocket. Dan jika dilihat dari nilai latensinya
saja, WebTransport Datagram lebih mudah diprediksi ketimbang protokol lainnya.

Terlihat bahwa mayoritas nilai RTT dari WebTransport lebih kecil dari pada WebSocket. Dan juga nilai
RTT WebSocket lebih sporadis dengan semakin besarnya ukuran \foreign{payload}.

\begin{figure}[H]
	\centering{}
	\resizebox{\columnwidth}{!}{\input{images/graphs/50mbit_1loss_rtt_big.tex}}
	\caption{RTT \foreign{Payload} Besar dengan 1\% \foreign{Packet Loss}}
	\label{fig:1loss_rtt_big}
\end{figure}

\subsection{Analisis Efek \foreign{Packet Loss}}
\label{subsec:hasil_packet_loss}

Dari pengujian pengiriman data pada berbagai tingkat \foreign{packet loss} dengan \foreign{delay} 20
ms. Didapatkan hasil yang menunjukan bahwa protokol WebTransport lebih toleran terhadap
\foreign{packet loss} ketimbang dengan WebSocket. Grafik pada \cref{fig:50mbit_0to10loss}
memperlihatkan pengaruh \foreign{packet loss} terhadap rata-rata nilai RTT. Terlihat bahwa kenaikan
nilai pada WebTransport lebih landai ketimbang WebSocket. Ini dapat berarti bahwa latensi protokol
WebSocket lebih mudah terpengaruh oleh \foreign{packet loss}.

\begin{figure}[ht]
	\centering{}
	\resizebox{\columnwidth}{!}{\input{images/graphs/loss_to_rtt.tex}}
	\caption{Pengaruh \foreign{Packet Loss} Terhadap RTT \foreign{Payload} 128 Byte}
	\label{fig:50mbit_0to10loss}
\end{figure}

WebTransport Stream juga dapat terlihat terpengaruh oleh \foreign{packet loss}, namun tidak sebesar
WebSocket. Dan data yang dikirim menggunakan WebTransport Datagram tidak berubah terlalu besar dari
20 ms, yang dapat berarti nilai latensinya tidak terpengaruh oleh \foreign{packet loss}. Akan
tetapi, \cref{fig:loss_to_rtt} menunjukan bahwa dengan semakin besarnya tingkat \foreign{packet
	loss}, jumlah datagram yang berhasil terkirim semakin menurun. Tingkat penurunan ini kurang lebih
sama dengan nilai \foreign{packet loss}-nya.

\begin{figure}[H]
	\centering{}
	\input{images/graphs/50mbit_loss_to_dgram.tex}
	\caption{Pengaruh \foreign{Packet Loss} Terhadap Jumlah Datagram Terkirim}
	\label{fig:loss_to_rtt}
\end{figure}

Perlu dicatat juga, nilai rata-rata RTT ini diambil dari data dengan distribusi \foreign{tail-heavy}
akibat dari waktu retransmisi dari paket yang gagal diterima. Akan tetapi, dengan semakin besarnya
nilai \foreign{packet loss}, probabilitas dari retransmisi juga turut meningkat. Karena ini
representasi nilai rata-rata tersebut dapat tetap menunjukan tingkat toleransi protokol terhadap
\foreign{packet loss}.

Pengaruh ini juga dapat terlihat pada \foreign{goodput} seperti yang ditunjukan
\cref{fig:loss_to_goodput}. Pada jaringan yang tidak memiliki \foreign{packet loss}, WebTransport
Stream memiliki nilai \foreign{goodput} yang lebih besar pada 5,25 MiB/s ketimbang WebSocket pada
4,92 MiB/s. Ketika \foreign{packet loss} muncul, terjadi penurunan pada semua protokol. Dan pada
setiap tingkatan \foreign{packet loss}, WebTransport memiliki nilai \foreign{goodput} lebih tinggi
sebesar 0,16 -- 0,22 MiB/s.

\begin{figure}[H]
	\centering{}
	\input{images/graphs/50mbit_loss_to_goodput.tex}
	\caption{Pengaruh \foreign{Packet Loss} Terhadap \foreign{Goodput}}
	\label{fig:loss_to_goodput}
\end{figure}

\Cref{fig:goodput_diff} menunjukan persentase peningkatan nilai \foreign{goodput} yang diberikan
WebTransport dari WebSocket yang dimana dengan semakin tingginya \foreign{packet loss}, peningkatan
tersebut juga semakin membesar. Pada tingkatan \foreign{loss} 0\%, nilai \foreign{goodput} dari
WebTransport 5\% lebih besar dari WebSocket dan pada \foreign{loss} 15\% \foreign{goodput}
WebTransport bernilai dua kali lipat nilai WebSocket.

\begin{figure}[H]
	\centering{}
	\input{images/graphs/goodput_diff.tex}
	\caption{Peningkatan \foreign{Goodput} dari WebSocket ke WebTransport}
	\label{fig:goodput_diff}
\end{figure}

\subsection{Analisis Hasil Simulasi \foreign{Head-of-Line} (HOL) \foreign{Blocking}}
\label{subsec:hasil_holb}

Hasil pengukuran dari simulasi HOL \foreign{blocking} pada \cref{tbl:holb_0loss} untuk jaringan
tanpa \foreign{packet loss}, menunjukan pengaruh dari \foreign{multiplexing} WebTransport dalam
optimasi pengiriman data konkuren jika dibandingkan dengan WebSocket. Pada WebSocket, selain dari
nilai rata-rata RTT yang secara umum lebih besar, RTT dari \foreign{payload} kecil juga relatif
bernilai cukup jauh dari \foreign{delay} jaringan (20 ms). Berbeda dengan WebTransport pada nomor 1
dan 2 yang memiliki rata-rata RTT \foreign{payload} kecil lebih dekat dengan 20 ms. Hal ini dapat
menunjukan bahwa HOL \foreign{blocking} terjadi pada WebSocket yang dimana pengiriman
\foreign{payload} besar menghambat pengiriman \foreign{payload} yang kecil. Dan pada WebTransport,
\foreign{blocking} ini tidak terjadi.

\begin{table}[ht]
	\centering{}
	\caption{RTT pada Simulasi HOL \foreign{Blocking} tanpa \foreign{Packet Loss}}
	\label{tbl:holb_0loss}
	\begin{tabular}{|c|c|c|r|}
		\hline
		\textbf{No.}       & \multicolumn{1}{c|}{\textbf{Ukuran Payload}} & \textbf{Protokol}                    & \multicolumn{1}{c|}{\(\pmb{\overline{\mathit{RTT}}}\)} \\ \hline
		\multirow{2}{*}{1} & 128 B                                        & WebTransport Datagram                & 22.458                                                 \\ \cline{2-4}
		                   & 64 KiB                                       & WebTransport Stream                  & 43.279                                                 \\ \hline
		\multirow{2}{*}{2} & 128 B                                        & \multirow{2}{*}{WebTransport Stream} & 23.636                                                 \\ \cline{2-2} \cline{4-4}
		                   & 64 KiB                                       &                                      & 43.327                                                 \\ \hline
		\multirow{2}{*}{3} & 128 B                                        & \multirow{2}{*}{WebSocket}           & 35.366                                                 \\ \cline{2-2} \cline{4-4}
		                   & 64 KiB                                       &                                      & 53.293                                                 \\ \hline
	\end{tabular}
\end{table}

Efek \foreign{blocking} ini juga dapat terlihat pada simulasi yang dipicu oleh \foreign{packet loss}
seperti yang ditunjukan oleh \cref{tbl:holb_10loss}. Pada tabel ini, dicantumkan dua kolom untuk
membantu dalam analisis dari efek \foreign{blocking} yang terjadi. Kolom \(\mathit{UF}\)
(\foreign{upper fence}) yang berisi nilai batas atas interval dari \foreign{boxplot} pada set data
\(\mathit{RTT}\). Dan kolom \(\lvert{} \mathit{UO} \rvert{}\) (\foreign{upper outliers}) yang berisi
jumlah nilai RTT yang lebih besar dari atau sama dengan \(\mathit{UF}\), berikut definisinya,
\begin{equation}
	\mathit{UO} = \{x \in{} \mathit{RTT} \mid{} x \geq{} \mathit{UF}\}
\end{equation}

\begin{table}[ht]
	\centering{}
	\caption{RTT pada Simulasi HOL \foreign{Blocking} dengan \foreign{Loss Rate} 10\%}
	\label{tbl:holb_10loss}
	\begin{tabular}{|c|c|r|r|r|}
		\hline
		\textbf{No.}       & \textbf{Protokol}                    & \multicolumn{1}{c|}{\(\pmb{\overline{\mathit{RTT}}}\)} & \multicolumn{1}{c|}{\(\pmb{\mathit{UF}}\)} & \multicolumn{1}{c|}{\(\pmb{\lvert{} \mathit{UO} \rvert{}}\)} \\ \hline
		\multirow{2}{*}{1} & WebTransport Datagram                & 20,452                                                 & 20,800                                     & 893                                                          \\ \cline{2-5}
		                   & WebTransport Stream                  & 24,365                                                 & 29,371                                     & 961                                                          \\ \hline
		\multirow{2}{*}{2} & \multirow{2}{*}{WebTransport Stream} & 24,577                                                 & 23,322                                     & 949                                                          \\ \cline{3-5}
		                   &                                      & 24,006                                                 & 25,560                                     & 929                                                          \\ \hline
		\multirow{2}{*}{3} & \multirow{2}{*}{WebSocket}           & 88,088                                                 & 55,274                                     & 1872                                                         \\ \cline{3-5}
		                   &                                      & 88,901                                                 & 57,643                                     & 1863                                                         \\ \hline
	\end{tabular}
\end{table}

Pada percobaan nomor 1, nilai rata-rata RTT WebTransport Datagram terlihat tidak terlalu
terpengaruhi oleh \foreign{packet loss}. Dan pada WebTransport Stream, terjadi kenaikan nilai
rata-rata RTT yang diakibatkan oleh \foreign{packet loss} seperti yang sebelumnya dibahas pada
\cref{subsec:hasil_packet_loss} dengan \foreign{outlier} berjumlah 9,61\% dari total data yang
dikirim. Nilai tersebut hampir sama dengan tingkat \foreign{packet loss} pada jaringan. Hal ini
terjadi juga pada percobaan nomor 2 pada kedua arus data.

Percobaan nomor 3 menunjukan efek \foreign{blocking} pada protokol WebSocket. Rata-rata RTT
pada kedua arus data bernilai hampir dua kali lipat dari pengiriman data dengan satu arus yang
ditunjukan sebelumnya pada \cref{fig:50mbit_0to10loss}. Jumlah \foreign{outlier} juga terlihat
mendekati dua kali lipat dari tingkat \foreign{packet loss}.

Salah satu hal yang dapat menyebabkan perbedaan ini, seperti yang sebelumnya dibahas pada
\cref{subsec:holb_measurement}, terdapat pada \foreign{multiplexing} pada WebTransport dan QUIC.
Pada WebTransport, pengiriman data ini dapat dimultipleks menjadi dua \foreign{stream} dalam satu
koneksi agar kedua arus data tersebut dapat dikirim dan diterima secara independen. Dan pada
WebSocket pengiriman kedua ukuran data tersebut dilakukan pada satu koneksi TCP saja dan tidak ada
\foreign{multiplexing} walaupun data tersebut dapat dianggap tidak bergantung satu sama lain.
Sehingga, pengiriman data harus ikut menunggu pengiriman data pada arus lain dalam satu antrian.

