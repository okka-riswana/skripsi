\section{Tinjauan Pustaka}
\label{sec:tinjauan_pustaka}

\subsection{Protokol QUIC}
\label{subsec:protokol_quic}

QUIC adalah protokol transport yang dibangun di atas UDP yang merupakan campuran TCP dan TLS yang
disatukan menjadi satu protokol. Misalnya, QUIC sama seperti TCP karena memiliki pengiriman data
yang andal \foreign{reliable} dengan \foreign{congestion control} dan \foreign{flow control} yang
ketat. QUIC juga memiliki keamanan data bawaan dengan mewajibkan penggunaan TLS serta
\foreign{multiplexing} aliran untuk memisahkan satu atau lebih arus data yang berbeda
\parencite{Yu2021}. Protokol QUIC ini dibuat diatas UDP untuk mengambil beberapa fitur yang umum
digunakan dalam TCP seperti \foreign{session} dan enkripsi menggunakan TLS tanpa membawa banyak
\foreign{overhead} dan kekurangannya.

Berbeda dengan TCP, QUIC memiliki sistem \foreign{handshake} yang relatif lebih efisien karena
penggunaan UDP sebagai basisnya. UDP merupakan protokol yang \foreign{connectionless} sehingga kedua
ujung dari jalur komunikasi tidak perlu membangun koneksi terlebih dahulu dan cukup mengirimkan
pesan atau data secara langsung. Dan implementasi QUIC ini berada pada \foreign{user space}, berbeda
dengan TCP/UDP yang mayoritas berada pada tingkatan sistem operasi atau \foreign{kernel space}. Hal
ini dapat mempermudah konfigurasi aplikasi atau server pada tingkatan lebih dekat dengan jaringan
dan juga memungkinkan adanya beberapa server dengan konfigurasi \foreign{transport layer} berbeda
secara bersamaan.

\begin{figure}[ht]
	\centering{}
	\begin{subfigure}[b]{0.45\columnwidth}
		\resizebox{\textwidth}{!}{\input{images/quic_1rtt_handshake.tex}}
		\caption{1-RTT}
		\label{fig:quic_1rtt_handshake}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\columnwidth}
		\resizebox{\textwidth}{!}{\input{images/quic_0rtt_handshake.tex}}
		\caption{0-RTT}
		\label{fig:quic_0rtt_handshake}
	\end{subfigure}
	\caption{\foreign{Handshake} Protokol QUIC}
	\label{fig:quic_handshake}
\end{figure}

QUIC memiliki dua metode \foreign{handshake} \parencite{Sy2019}. Yang pertama adalah 1-RTT
\foreign{handshake} (\cref{fig:quic_1rtt_handshake}) yang berlangsung selama satu
\foreign{Round-Trip Time} (RTT) dan dilakukan jika klien-server tidak pernah melakukan koneksi
sebelumnya dan juga data-data koneksi dari keduanya masih valid. Jika koneksi klien-server sudah
pernah dilakukan sebelumnya, maka klien dapat secara langsung mengirimkan data secara langsung ke
server, metode ini disebut 0-RTT (\cref{fig:quic_0rtt_handshake}) karena klien dan server tidak
perlu bertukar informasi mengenai koneksi seperti ID koneksi dan negosiasi \foreign{cipher} untuk
enkripsi pada TLS yang mengambil waktu ketika pembentukan koneksi barunya.

\subsection{Protokol WebTransport}
\label{subsec:protokol_webtransport}

WebTransport adalah protokol transportasi data dua arah yang memanfaatkan HTTP/3 dan QUIC. Protokol
ini memiliki API Datagram yang mengirim data dengan metode \foreign{best-effort},
\foreign{unreliable}, dan \foreign{unordered}, serta API Stream yang mengirim data secara
\foreign{reliable} dan \foreign{ordered}. Per Juni 2022, spesifikasi IETF untuk protokol
WebTransport masih berupa draf \parencite{ietf-webtrans-http3-02} begitu juga dengan spesifikasi Web
API dari WHATWG \parencite{Aboba2022}.

Datagram berguna untuk mengirim dan menerima data yang tidak memerlukan jaminan pengiriman tingkat
tinggi. Paket data individu dibatasi ukurannya oleh \foreign{Maximum Transmission Unit} (MTU)
koneksi yang mendasarinya, dan data yang dikirim mungkin tidak ditransmisikan dengan benar, dan jika
terkirim, data mungkin juga tiba dalam urutan yang sewenang-wenang. API datagram ini cocok untuk
transportasi data dengan latensi rendah. Datagram ini mirip dengan pesan protokol UDP, tetapi
dienkripsi dan berada dibawah \foreign{congestion control}.

API Stream, di sisi lain, memastikan bahwa data ditransfer dengan cara yang andal dan terurut. API
ini cocok untuk kasus di mana satu atau lebih aliran data terorganisir harus dikirim atau diterima.
Banyak karakteristik koneksi WebTransport Stream yang mirip dengan beberapa koneksi TCP, namun
karena HTTP/3 didasarkan pada protokol QUIC yang lebih ringan, setiap sesi dan \foreign{stream}
dapat dibuka dan ditutup dengan lebih sedikit \foreign{overhead}.

\subsection{Metrik Kinerja Jaringan}
\label{subsec:metrik_jaringan}

\foreign{Packet loss} dan \foreign{Round-Trip Time} (RTT) adalah dua metrik kinerja jaringan penting
yang mempengaruhi \foreign{goodput} suatu koneksi yang bersifat \foreign{reliable} seperti TCP
\parencite{Gridelli2020}. Pemantauan secara \foreign{real-time} terhadap kedua nilai ini pada dapat
membantu dalam meneliti karakteristik dari jaringan dan juga mengidentifikasi permasalahan
kelambatan yang dialami oleh aplikasi karena jaringan.

\foreign{Packet loss} disebabkan oleh paket yang salah diterima oleh tujuan atau paket yang diterima
sama sekali. \foreign{Packet loss} dihitung sebagai metrik persentase paket jaringan yang diterima
dengan cacat atau tidak diterima sama sekali. Pada protokol yang \foreign{reliable} seperti TCP,
paket yang gagal diterima ini akan diretransmisi oleh pengirim. Metrik ini merupakan pengukuran
kualitas jaringan yang baik karena secara akurat mencerminkan reliabilitas jaringan
\parencite{Aidarous2003}.

\begin{figure}[ht]
	\centering{}
	\input{images/rtt.tex}
	\caption{\foreign{Round-Trip Time}}
	\label{fig:rtt}
\end{figure}

\foreign{Round-Trip Time} atau RTT merupakan waktu yang dibutuhkan sebuah paket data untuk
bolak-balik ke tujuan tertentu (\cref{fig:rtt}). Nilai RTT ini tergantung pada latensi yang terjadi
pada setiap \foreign{hop} (termasuk \foreign{host} tujuan), kecepatan pemrosesan data, dan faktor
lainnya \parencite{Indrarini2018}. \foreign{Packet loss} juga dapat mempengaruhi nilai RTT, salah
satu penyebabnya adalah dari kasus \foreign{Head-of-Line} (HOL) \foreign{blocking} yang dimana suatu
paket terhambat pengirimannya karena retransmisi untuk paket yang sebelumnya gagal dikirim.
\Cref{fig:holb} menunjukan penghambatan ini.

\begin{figure}[ht]
	\centering{}
	\input{images/holb.tex}
	\caption{\foreign{Head-of-Line Blocking} Akibat \foreign{Packet Loss}}
	\label{fig:holb}
\end{figure}

Metrik lain yang dipantau pada penelitian ini adalah \foreign{goodput}. \foreign{Goodput} adalah
kecepatan sebenarnya yang dialami oleh \foreign{hosts} ketika melakukan transfer data pada tingkat
aplikasi \parencite{Mills2011}. Metrik ini hanya menghitung data yang berguna saja dan tidak
menghitung retransmisi dari paket yang gagal dikirim. Dan faktor seperti \foreign{overhead} dari
protokol \foreign{transport-layer} juga dapat mempengaruhi nilai dari \foreign{goodput} ini.

Nilai \foreign{goodput} berbeda dengan \foreign{throughput} dan \foreign{bandwidth}.
\foreign{Throughput} adalah jumlah data aktual yang ditransfer antara sumber dan \foreign{host}
tujuan per satuan waktu. \foreign{Bandwidth} adalah nilai throughput maksimum teoritis yang dapat
dicapai pada jaringan dan umumnya ditentukan oleh media transmisi.

