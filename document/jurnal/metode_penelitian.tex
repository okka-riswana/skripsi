\section{Metode Penelitian}
\label{sec:metode_penelitian}

Penelitian ini akan berfokus kepada \foreign{Round-Trip Time} (RTT) dari data yang dikirim
menggunakan protokol WebTransport dalam berbagai nilai \foreign{packet loss} dengan protokol
WebSocket sebagai acuan. Penelitian akan dilakukan pada lingkungan yang terkontrol. Aplikasi klien
dan server akan dijalankan pada \foreign{Virtual Machine} (VM) dengan jaringan virtual.

Untuk menyimulasikan berbagai kondisi jaringan, jaringan akan diberikan \foreign{packet loss} dan
juga \foreign{bandwidth} yang telah dikontrol secara artifisial menggunakan Linux \foreign{Traffic
Control} (tc) dan netem. Pengukuran akan dilakukan pada dua VM dalam satu komputer \foreign{host}
dengan jaringan virtual dan menggunakan \foreign{Kernel Virtual Machine} (KVM) sebagai
\foreign{hypervisor}-nya. Komputer \foreign{host} memiliki prosesor AMD Ryzen 5900X dengan 12
Core/24 Thread, RAM 32 GiB, dan NVME SSD dan menjalankan sistem operasi Fedora GNU/Linux versi 36
dengan kernel versi 5.17. Setiap VM memiliki 4 CPU yang di-\foreign{pin} untuk VM tersebut dan juga
RAM sebesar 4 GiB. \Cref{fig:testbed_diagram} memperlihatkan struktur dari \foreign{test bed} pada
penelitian ini.

\begin{figure}[ht]
	\centering{}
	\resizebox{\columnwidth}{!}{\input{images/testbed_diagram.tex}}
	\caption{Diagram \foreign{Test Bed}}
	\label{fig:testbed_diagram}
\end{figure}

Jaringan untuk VM memiliki MTU 1500 byte dan diberi \foreign{bandwidth} tetap sebesar 50 mbps
menggunakan tc dengan \foreign{queueing discipline} (qdisc) \foreign{Hierarchy Token Bucket} (HTB)
agar tidak bergantung kepada karakteristik \foreign{bandwidth} pada tingkatan \foreign{interface}
\parencite{Devera2002}. Kemudian diatas limitasi \foreign{bandwidth} tersebut, ditambahkan
\foreign{delay} selama 20 milidetik menggunakan netem yang bertujuan sebagai simulasi pada
komunikasi jaringan di dunia nyata dan juga untuk meminimalisir fluktuasi dari beban CPU.

\subsection{Pengukuran RTT}
\label{subsec:latency_measurement}

Nilai RTT pada pengukuran ini dihitung dari jarak waktu antara pengiriman data dan data diterima
kembali. Pengukuran ini dilakukan dengan mengirim data 10,000 kali secara sinkronus, yang dimana
data baru akan dikirim ketika data sebelumnya sudah diterima atau melebihi \foreign{timeout}.
Jaringan yang digunakan memiliki \foreign{packet loss rate} sebesar 1\% yang diaplikasikan untuk
mengobservasi karakteristik dari setiap protokol ketika \foreign{packet loss} terjadi.

Pengukuran ini dilakukan pada dua jenis \foreign{payload}. Yang pertama adalah \foreign{payload}
berukuran 128 byte sebagai simulasi data kecil dibawah ukuran MTU (1500 byte) dan dapat dikirim
dalam satu \foreign{frame} pada \foreign{data-link layer}. Yang kedua adalah \foreign{payload}
dengan data dengan ukuran diatas 32 KiB, yang dimana \foreign{payload} ini akan dipecah menjadi
beberapa \foreign{frame} oleh protokol yang mendasarinya (QUIC untuk WebTransport dan TCP untuk
WebSocket).

Untuk pengukuran latensi \foreign{payload} dengan ukuran diatas 1 KiB, protokol yang dipakai adalah
WebTransport Stream dan WebSocket. WebTransport Datagram tidak dapat digunakan karena batasan dari
ukuran data yang harus lebih kecil dari nilai MTU dikurangi dengan \foreign{overhead} protokol
dibawahnya (seperti QUIC, UDP, dan IP). Dari limitasi ukuran tersebut, data yang dapat dikirim
melalui WebTransport Datagram pada penelitian ini memiliki ukuran maksimal 1,218 byte.

\subsection{Pengujian Efek \foreign{Packet Loss}}
\label{subsec:loss_effects_measurement}

Pengujian ini bertujuan untuk mengukur toleransi protokol terhadap \foreign{packet loss}. Metode
yang digunakan disini kurang lebih sama dengan metode yang digunakan pada pengukuran RTT, namun
tidak hanya dilakukan pada tingkat \foreign{loss} 1\% saja. Disini nilai RTT akan diukur pada
jaringan dengan \foreign{loss rate} 0\% sampai 15\% dengan kenaikan 1\%.

Ukuran \foreign{payload} yang digunakan adalah 128 byte dan 1 MiB. \foreign{Payload} 128 byte
digunakan untuk melihat efek dari \foreign{packet loss} terhadap latensi dari setiap protokol. Dan
\foreign{payload} 1 MiB digunakan untuk mengukur \foreign{goodput} pada protokol WebTransport Stream
dan WebSocket. Pengukuran \foreign{goodput} ini juga tidak dilakukan pada WebTransport Datagram
karena batasan ukuran seperti yang dijelaskan sebelumnya pada \cref{subsec:latency_measurement}.
Nilai \foreign{goodput} ini dihitung dari setengah nilai rata-rata RTT \foreign{payload} 1 MiB.

\subsection{Pengukuran Nilai RTT pada Simulasi \foreign{Head-of-Line Blocking}}
\label{subsec:holb_measurement}

Tujuan utama dari pengukuran ini adalah untuk menguji fitur \foreign{multiplexing} dari
WebTransport, atau lebih tepatnya pada protokol QUIC dibawahnya. Pengujian ini juga bertujuan untuk
melihat performa protokol dalam memproses arus data konkuren. Untuk menyimulasikan dua situasi yang
dapat memicu \foreign{Head-of-Line} (HOL) \foreign{blocking}.

Yang pertama pengiriman dua buah \foreign{payload} dengan ukuran data berbeda secara konkuren pada
satu koneksi/sesi. Data pertama berukuran 64 KiB yang jauh lebih besar dari data kedua yang
berukuran 128 byte. Data yang besar tersebut akan membutuhkan waktu yang lebih lama untuk dikirim
ketimbang data yang kecil, sehingga pada arus data tunggal, data yang kecil akan terblokir
pengirimannya. Situasi yang kedua adalah dengan mengirimkan data dua berukuran 128 byte sebanyak
10.000 kali secara konkuren dengan pada jaringan dengan tingkat \foreign{packet loss} 10\%. Situasi
ini dapat memicu \foreign{blocking} pada satu arus data WebTransport Stream dan WebSocket karena
perlunya waktu retransmisi jika data tidak berhasil terkirim.

Fitur \foreign{multiplexing} dari WebTransport bertujuan untuk menghilangkan atau meminimalisir efek
dari HOL \foreign{blocking} dengan membuat dua atau lebih arus data yang independen pada satu
koneksi. Salah satu contoh dari efek \foreign{blocking} ini pada aplikasi dunia nyata adalah video
game \foreign{multiplayer} yang membutuhkan dua atau lebih arus data yang harus dikomunikasikan
\parencite{Lepola2020}. Sebagai contoh, data posisi pemain yang membutuhkan latensi sekecil mungkin
dan fitur komunikasi \foreign{chat} yang menjadi fitur sekunder. Jika arus data pada \foreign{chat}
cukup besar atau terjadi \foreign{loss} pada data yang dikirim, maka arus data posisi pemain dapat
ikut terhambat.
