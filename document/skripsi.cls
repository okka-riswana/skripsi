%-------------------------------------------------------------------------------
% Document Class Skripsi untuk FMIPA Unpad
%-------------------------------------------------------------------------------
%
% Copyright 2022 Okka Riswana
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
%-------------------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{skripsi}

\newif{\ifmsthesis}
\newif{\ifmsproposal}
\DeclareOption{skripsi}{\msthesistrue\msproposalfalse}
\DeclareOption{proposal}{\msthesisfalse\msproposaltrue}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ExecuteOptions{skripsi}
\ProcessOptions{}
\LoadClass[12pt]{report}

%-------------------------------------------------------------------------------
% Packages
%-------------------------------------------------------------------------------

\RequirePackage[main = indonesian, english]{babel}
\RequirePackage[style=british]{csquotes}
\RequirePackage[T1]{fontenc}
\RequirePackage{mathptmx}
\RequirePackage[scaled = 0.9]{couriers}
\RequirePackage{microtype}
\RequirePackage{graphicx}
\RequirePackage{verbatim}
\RequirePackage{enumerate}
\RequirePackage{latexsym}
\RequirePackage{array}
\RequirePackage[table]{xcolor}
\RequirePackage{tabularx}
\RequirePackage{multirow}
\RequirePackage{float}
\RequirePackage{indentfirst}
\RequirePackage{setspace}
\RequirePackage{enumitem}
\RequirePackage[style = apa, backend = biber, datelabel = year]{biblatex}
\RequirePackage[titles]{tocloft}
\RequirePackage[pdfusetitle]{hyperref}
\RequirePackage[labelsep=space]{caption}
\RequirePackage{subcaption}
\RequirePackage{tikz}
\RequirePackage{geometry}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage[nameinlink]{cleveref}

\usetikzlibrary{calc}

%-------------------------------------------------------------------------------
% Redefine caption names
%-------------------------------------------------------------------------------

% Indonesian
\def\captionsindonesian{%
	\def\prefacename{KATA PENGANTAR}%
	\def\contentsname{DAFTAR ISI}%
	\def\listfigurename{DAFTAR GAMBAR}%
	\def\listtablename{DAFTAR TABEL}%
	\def\listappendixname{DAFTAR LAMPIRAN}%
	\def\nomenclaturename{DAFTAR SINGKATAN}%
	\def\abstractname{ABSTRAK}%
	\def\acknowledgmentname{HALAMAN PERSEMBAHAN}%
	\def\approvalname{HALAMAN PENGESAHAN}
	\def\partname{BAGIAN}%
	\def\chaptername{BAB}%
	\def\appendixname{LAMPIRAN}%
	\def\refname{DAFTAR PUSTAKA}%
	\def\bibname{DAFTAR PUSTAKA}%
	\def\indexname{Indek}%
	\def\figurename{Gambar}%
	\def\tablename{Tabel}%
	\def\pagename{Halaman}%
}

% English
\def\captionsenglish{%
	\def\prefacename{PREFACE}%
	\def\contentsname{CONTENTS}%
	\def\listfigurename{LIST OF FIGURES}%
	\def\listtablename{LIST OF TABLES}%
	\def\listappendixname{LIST OF APPENDICES}%
	\def\nomenclaturename{NOMENCLATURE}%
	\def\abstractname{\emph{ABSTRACT}}%
	\def\partname{PART}%
	\def\chaptername{CHAPTER}%
	\def\appendixname{APPENDIX}%
	\def\refname{REFERENCES}%
	\def\bibname{REFERENCES}%
	\def\indexname{Index}%
	\def\figurename{Figure}%
	\def\tablename{Table}%
	\def\pagename{Page}%
}

\crefname{figure}{Gambar}{Gambar-gambar}
\Crefname{figure}{Gambar}{Gambar-gambar}
\crefname{table}{Tabel}{Tabel-tabel}
\Crefname{table}{Tabel}{Tabel-tabel}
\crefname{equation}{persamaan}{persamaan-persamaan}
\Crefname{equation}{Persamaan}{Persamaan-persamaan}
\creflabelformat{equation}{#2#1#3}
\crefname{chapter}{bab}{bab-bab}
\Crefname{chapter}{Bab}{Bab-bab}
\crefname{section}{subbab}{subbab-subbab}
\Crefname{section}{Subbab}{Subbab-subbab}
\crefname{subsection}{subsubbab}{subsubbab-subsubbab}
\Crefname{subsection}{Subsubbab}{Subsubbab-subsubbab}
\crefname{appendix}{lampiran}{lampiran-lampiran}
\Crefname{appendix}{Lampiran}{Lampiran-lampiran}

%-------------------------------------------------------------------------------
% Define thesis's input variables
%-------------------------------------------------------------------------------

\gdef\university{Universitas Padjadjaran}
\gdef\faculty{Matematika dan Ilmu Pengetahuan Alam}
\gdef\city{Sumedang}

\newcommand{\settitleind}[1]{\title{#1}\gdef\titleind{#1}}
\newcommand{\titleind}{}
\newcommand{\settitleeng}[1]{\gdef\titleeng{#1}}
\newcommand{\titleeng}{}
\newcommand{\setkeywordsind}[1]{\hypersetup{pdfkeywords={#1}}\gdef\keywordsind{#1}}
\newcommand{\keywordsind}{}
\newcommand{\setkeywordseng}[1]{\gdef\keywordseng{#1}}
\newcommand{\keywordseng}{}
\newcommand{\setfullname}[1]{\author{#1}\gdef\fullname{#1}}
\newcommand{\fullname}{}
\newcommand{\setidnum}[1]{\gdef\idnum{#1}}
\newcommand{\idnum}{}
\newcommand{\setexamdate}[1]{\gdef\examdate{#1}}
\newcommand{\examdate}{\number\day~\ifcase\month\or
		Januari\or Pebruari\or Maret\or April\or Mei\or Juni\or
		Juli\or Agustus\or September\or Oktober\or Nopember\or Desember\fi
	\space \number\year}
\newcommand{\setapprovaldate}[1]{\gdef\approvaldate{#1}}
\newcommand{\setdegree}[1]{\gdef\degree{#1}}
\newcommand{\degree}{}
\newcommand{\setyearsubmit}[1]{\gdef\yearsubmit{#1}}
\newcommand{\yearsubmit}{}
\newcommand{\setprogram}[1]{\gdef\program{#1}}
\newcommand{\program}{}
\newcommand{\setdept}[1]{\gdef\dept{#1}}
\newcommand{\dept}{}

%-------------------------------------------------------------------------------
% Layout
%-------------------------------------------------------------------------------

\geometry{
	a4paper,
	papersize = {215mm, 280mm},
	top = 4cm,
	left = 4cm,
	bottom = 3cm,
	right = 3cm,
	marginpar = 0pt,
	marginparwidth = 0pt,
	headsep = 2cm
}
\setlength{\parindent}{2.5em} % Indent approx 5 character

%-------------------------------------------------------------------------------
% Table of contents and hyperlink
%-------------------------------------------------------------------------------

\hypersetup{pdfborder=0 0 0}

\newlength{\mylenf}
\settowidth{\mylenf}{\cftfigpresnum}
\addtolength{\cftfignumwidth}{\dimexpr\mylenf+3.5em}
\addtolength{\cfttabnumwidth}{\dimexpr\mylenf+3.5em}
\renewcommand{\cftfigpresnum}{\figurename\space}
\renewcommand{\cfttabpresnum}{\tablename\space}

\setlength{\cfttabindent}{0pt}
\setlength{\cftfigindent}{0pt}

\renewcommand{\cftchappresnum}{\chaptername\space}
\newlength\chaplen
\setlength{\chaplen}{4em}
\cftsetindents{chap}{0pt}{\chaplen}
\renewcommand{\cftchapfont}{\normalsize\normalfont}
\renewcommand{\cftchappagefont}{\normalsize\normalfont}

\newlength\sectionlen
\setlength{\sectionlen}{2.5em}
\cftsetindents{section}{0pt}{\sectionlen}

\newlength\subsectionlen
\setlength{\subsectionlen}{3em}
\cftsetindents{subsection}{\sectionlen}{\subsectionlen}

\renewcommand{\cftchapleader}{\cftdotfill{\cftdotsep}}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}

\newlength{\tocskip}
\setlength{\tocskip}{\lineskip}
\setlength{\cftbeforechapskip}{\tocskip}
\setlength{\cftbeforesecskip}{\tocskip}
\setlength{\cftbeforesubsecskip}{\tocskip}
\setlength{\cftbeforetabskip}{\tocskip}
\setlength{\cftbeforefigskip}{\tocskip}

\newlistof{appendix}{app}{\listappendixname}
\setcounter{appdepth}{2}
\renewcommand{\theappendix}{\arabic{appendix}}
\renewcommand{\cftappendixpresnum}{Lampiran\space}
\setlength{\cftbeforeappendixskip}{\tocskip}
\setlength{\cftappendixnumwidth}{\cftfignumwidth}

\newlistentry[appendix]{subappendix}{app}{1}
\renewcommand{\thesubappendix}{\theappendix.\arabic{subappendix}}
\renewcommand{\cftsubappendixpresnum}{Lampiran\space}
\setlength{\cftbeforesubappendixskip}{\tocskip}
\setlength{\cftsubappendixnumwidth}{\cftfignumwidth}
\setlength{\cftsubappendixindent}{0pt}

%-------------------------------------------------------------------------------
% Page numbering
%-------------------------------------------------------------------------------

\markright{}

% Define page numbering in the first chapter
\def\ps@chapterheading{%
	\let\@evenhead\@empty\let\@oddhead\@empty
	\def\@oddfoot{\hfil\thepage\hfil}%
	\def\@evenfoot{\hfil\thepage\hfil}
}

%-------------------------------------------------------------------------------
% Enumerations
%-------------------------------------------------------------------------------

\setlist{leftmargin=*, labelsep=*, leftmargin=\parindent, itemsep=-2pt}

%-------------------------------------------------------------------------------
% Redefine chapter and sections
%-------------------------------------------------------------------------------

% Chapter/section/subsection numbering
\setcounter{secnumdepth}{2}
\renewcommand{\thepart}{\@Roman\c@part}
\renewcommand{\thechapter}{\@Roman\c@chapter}
\renewcommand{\thesection}{\@arabic\c@chapter.\@arabic\c@section}
\renewcommand{\thesubsection}{\@arabic\c@chapter.\@arabic\c@section.\@arabic\c@subsection}

\renewcommand{\chapter}{\clearpage\thispagestyle{chapterheading}%
	\global\@topnum\z@ % Prevents figures from going at top of page
	\@afterindenttrue % Indent the 1st paragraph
	\secdef\@chapter\@schapter}

% Non-content chapters header (table of contents, abstract, etc.)
\renewcommand{\@makechapterhead}[1]{{
			\parindent \z@ \centering \normalfont
			\ifnum \c@secnumdepth >\m@ne
				\bfseries \@chapapp\space \thechapter
				\par\nobreak
				\vskip 5\p@
			\fi
			\interlinepenalty\@M
			\bfseries
			#1\par\nobreak
			\vskip 20\p@
		}}

% Chapters header (BAB)
\renewcommand{\@makeschapterhead}[1]{{
			\parindent \z@ \centering \normalfont
			\interlinepenalty\@M \bfseries #1\par\nobreak \vskip 20\p@
		}}

% Section header
\renewcommand{\section}{\@startsection{section}{1}{\z@}%
	{-3.5ex \@plus -1ex \@minus -.2ex}%
	{\lineskip}%
	{\normalfont\normalsize\bfseries}}

% Subsection header
\renewcommand{\subsection}{\@startsection{subsection}{2}{\z@}%
	{-3.25ex \@plus -1ex \@minus -.2ex}%
	{\lineskip}%
	{\normalfont\normalsize\bfseries}}

% Subsubsection header
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{\z@}%
	{3.25ex \@plus 1ex \@minus.2ex}%
	{\lineskip}%
	{\normalfont\normalsize\bfseries}}

\renewcommand{\paragraph}{\subparagraph}

% Equations, tables, figures
\@addtoreset{equation}{chapter}
\renewcommand{\theequation}{\ifnum \c@chapter>\z@ \@arabic\c@chapter.\fi \@arabic\c@equation}
\renewcommand{\thefigure}{\ifnum \c@chapter>\z@ \@arabic\c@chapter.\fi \@arabic\c@figure}
\renewcommand{\thetable}{\ifnum \c@chapter>\z@ \@arabic\c@chapter.\fi \@arabic\c@table}

% Appendix
\newcommand{\myappendix}[1]{
	\refstepcounter{appendix}
	\section*{Lampiran \theappendix\space #1}
	\addcontentsline{app}{appendix}{\protect\numberline{\theappendix}#1}
	\par{}
}

% Subappendix
\newcommand{\subappendix}[1]{
	\refstepcounter{subappendix}
	\subsection*{Lampiran \thesubappendix\space #1}
	\addcontentsline{app}{subappendix}{\protect\numberline{\thesubappendix}#1}
}

%-------------------------------------------------------------------------------
% Tabularization
%-------------------------------------------------------------------------------

\newcommand{\itab}[1]{\hspace{0em}\rlap{#1}}
\newcommand{\tab}[1]{\hspace{.2\textwidth}\rlap{#1}}

\newcolumntype{+}{>{\global\let\currentrowstyle\relax}}
\newcolumntype{^}{>{\currentrowstyle}}
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}#1\ignorespaces}

%-------------------------------------------------------------------------------
% Spacing
%-------------------------------------------------------------------------------

\doublespacing{}

%-------------------------------------------------------------------------------
% Hyperlink whole citation parentheses content
%-------------------------------------------------------------------------------

\DeclareFieldFormat{bibhyperrefnonest}{\DeclareFieldFormat{bibhyperref}{##1}\bibhyperref{#1}}

\DeclareCiteCommand{\parencite}[\mkbibparens]
{\usebibmacro{cite:init}\usebibmacro{prenote}}
{\usebibmacro{citeindex}\printtext[bibhyperrefnonest]{\usebibmacro{cite}}}
{}
{\usebibmacro{postnote}\usebibmacro{cite:post}}

\DeclareCiteCommand{\textcite}
{\usebibmacro{cite:init}\usebibmacro{prenote}}
{\usebibmacro{citeindex}\printtext[bibhyperrefnonest]{\usebibmacro{textcite}}}
{}
{\usebibmacro{textcite:postnote}\usebibmacro{cite:post}}

\AtEveryCite{%
	\clearfield{labelmonth}%
	\clearfield{labelday}%
	\clearfield{labelendmonth}%
	\clearfield{labelendday}%
}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------

\endinput
