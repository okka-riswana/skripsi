-- vim: ft=lua:ts=2:sw=2:et:cc=80

local api = vim.api
local opt_local = vim.opt_local

api.nvim_create_autocmd('FileType', {
  group = api.nvim_create_augroup('UseTabInTex', {}),
  pattern = { 'tex', 'sty', 'cls' },
  callback = function()
    opt_local.wrap = true
    opt_local.textwidth = 100
    opt_local.tabstop = 4
    opt_local.shiftwidth = 4
    opt_local.expandtab = false
    opt_local.colorcolumn = { 100 }
  end,
})

api.nvim_create_autocmd('FileType', {
  group = api.nvim_create_augroup('UseSpaceInBib', {}),
  pattern = { 'bib' },
  callback = function()
    opt_local.tabstop = 2
    opt_local.shiftwidth = 2
    opt_local.expandtab = true
    opt_local.colorcolumn = { 80 }
  end,
})
