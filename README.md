# Undergrad Thesis

Performance Analysis of WebTransport Protocol Via Hypertext Transfer Protocol Version 3 (HTTP/3) and
QUIC on Web-Based Applications

> Okka Riswana\
  Padjadjaran University

Directories:
* [`document/`](document): Latex document sources
* [`client/`](client): JavaScript WebTransport test client sources
* [`impl-cpp`](impl-cpp): C++ implementation of WebTransport server based on minimized Chromium network stack from [NaiveProxy](https://github.com/klzgrad/naiveproxy).
